from setuptools import setup, find_packages

EXCLUDE_FROM_PACKAGES = ["test_keras", "test_keras.*"]


setup(name='carp',
      version='0.1',
      description='carp',
      url='https://gitlab.com/polavieja_lab/CARP_library.git',
      author='Francisco Romero-Ferrero',
      author_email='paco.romero.ferrero@gmail.com',
      license='GPL',
      packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
      scripts=['bin/build_carp.py', 'bin/run_algorithm_test.py',
               'bin/run_network_test.py', 'bin/tests_data_frame.py'],
      zip_safe=False)
