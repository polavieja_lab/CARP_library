from __future__ import absolute_import, division, print_function
# Import standard libraries
import os
import numpy as np
import time
import logging.config
import yaml
# Import third party libraries
from idtrackerai.constants import THRESHOLD_ACCEPTABLE_ACCUMULATION
from idtrackerai.video import Video
from carp.experiments.synthetic_list_of_blobs import BlobsListConfig,\
                                            generate_list_of_blobs,\
                                            subsample_dataset_by_individuals
from idtrackerai.list_of_blobs import ListOfBlobs
from idtrackerai.list_of_fragments import ListOfFragments,\
                                          create_list_of_fragments
from idtrackerai.list_of_global_fragments import ListOfGlobalFragments,\
                                                create_list_of_global_fragments
from idtrackerai.utils.GUI_utils import getInput
from idtrackerai.pre_trainer import pre_trainer
from idtrackerai.accumulation_manager import AccumulationManager
from idtrackerai.accumulator import accumulate
from idtrackerai.network.identification_model.network_params import NetworkParams
from idtrackerai.assigner import assigner
from idtrackerai.network.identification_model.id_CNN import ConvNetwork
from idtrackerai.groundtruth_utils.compute_groundtruth_statistics import get_accuracy_wrt_groundtruth
from idtrackerai.groundtruth_utils.generate_groundtruth import generate_groundtruth
from carp.experiments.global_fragments_statistics import compute_and_plot_fragments_statistics  # plot_fragments_generated


global logger
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# Constants


def setup_logging(default_path='logging.yaml',
                  default_level=logging.INFO,
                  env_key='LOG_CFG',
                  path_to_save_logs='./',
                  video_object=None):
    """Setup logging configuration
    """
    global logger
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        if os.path.exists(path_to_save_logs) and video_object is not None:
            video_object.logs_folder = os.path.join(path_to_save_logs,
                                                    'log_files')
            if not os.path.isdir(video_object.logs_folder):
                os.makedirs(video_object.logs_folder)
            config['handlers']['info_file_handler']['filename'] = \
                os.path.join(video_object.logs_folder, 'info.log')
            config['handlers']['error_file_handler']['filename'] = \
                os.path.join(video_object.logs_folder, 'error.log')
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

    logger = logging.getLogger(__name__)
    logger.propagate = True
    logger.setLevel("DEBUG")
    return logger


def create_video_logs_folder(video):
    if hasattr(video, 'session_folder')\
            and os.path.exists(video.session_folder)and video is not None:
        video.logs_folder = os.path.join(video.session_folder, 'logs')
        if not os.path.isdir(video.logs_folder):
            os.makedirs(video.logs_folder)
        return True
    return False


def init_video(video_folder, imsize, group_size,
               frames_in_video, knowledge_transfer_flag,
               knowledge_transfer_folder):
    video = Video()  # instantiate object video
    video.video_path = os.path.join(video_folder, '_fake_0.avi')  # set path
    video.create_session_folder()
    create_video_logs_folder(video)
    logger = setup_logging(path_to_save_logs=video.session_folder,
                           video_object=video)
    logger.info("Starting working on session %s" % video.session_folder)
    logger.info("Log files saved in %s" % video.logs_folder)
    video._number_of_animals = group_size
    video._maximum_number_of_blobs = group_size
    video._number_of_frames = frames_in_video
    video._tracking_with_knowledge_transfer = knowledge_transfer_flag
    video._knowledge_transfer_model_folder = knowledge_transfer_folder
    video._identification_image_size = (imsize, imsize, 1)
    return video


def preprocessing(video, group_size, scale_parameter, shape_parameter,
                  frames_in_video, repetition, dataset, starting_frame=None):
    print("\n*** Preprocessing")
    video.create_preprocessing_folder()
    list_of_blobs_config = BlobsListConfig(number_of_animals=group_size,
                                           scale_parameter=scale_parameter,
                                           shape_parameter=shape_parameter,
                                           number_of_frames=frames_in_video,
                                           repetition=repetition,
                                           starting_frame=starting_frame)
    identification_images, centroids = \
        subsample_dataset_by_individuals(dataset, list_of_blobs_config)
    blobs = generate_list_of_blobs(identification_images, centroids,
                                   list_of_blobs_config)
    video._has_been_segmented = True
    list_of_blobs = ListOfBlobs(blobs_in_video=blobs)
    logger.info("Computing maximum number of blobs detected in the video")
    list_of_blobs.check_maximal_number_of_blob(video.number_of_animals)
    logger.info("Computing a model of the area of the individuals")
    list_of_blobs.video = video
    list_of_blobs.compute_fragment_identifier_and_blob_index(
        video.number_of_animals)
    list_of_blobs.compute_crossing_fragment_identifier()
    fragments = create_list_of_fragments(list_of_blobs.blobs_in_video,
                                         video.number_of_animals)
    # plot_fragments_generated(fragments)
    list_of_fragments = ListOfFragments(fragments)
    video._fragment_identifier_to_index = \
        list_of_fragments.get_fragment_identifier_to_index_list()
    global_fragments = \
        create_list_of_global_fragments(list_of_blobs.blobs_in_video,
                                        list_of_fragments.fragments,
                                        video.number_of_animals)
    list_of_global_fragments = ListOfGlobalFragments(global_fragments)
    video.number_of_global_fragments = \
        list_of_global_fragments.number_of_global_fragments
    list_of_global_fragments.filter_candidates_global_fragments_for_accumulation()
    video.number_of_global_fragments_candidates_for_accumulation = \
        list_of_global_fragments.number_of_global_fragments
    video.individual_fragments_lenghts, \
        video.individual_fragments_distance_travelled, \
        video._gamma_fit_parameters = compute_and_plot_fragments_statistics(
            video, list_of_fragments=list_of_fragments,
            list_of_global_fragments=list_of_global_fragments,
            save=True, plot=False)
    video.number_of_global_fragments = \
        list_of_global_fragments.number_of_global_fragments
    list_of_global_fragments.filter_candidates_global_fragments_for_accumulation()
    video.number_of_global_fragments_candidates_for_accumulation = \
        list_of_global_fragments.number_of_global_fragments
    list_of_global_fragments.relink_fragments_to_global_fragments(
        list_of_fragments.fragments)
    video._number_of_unique_images_in_global_fragments = \
        list_of_fragments.compute_total_number_of_images_in_global_fragments()

    return video, list_of_blobs, list_of_fragments, list_of_global_fragments,\
        list_of_blobs_config


def extra_preprocessing_steps(video, list_of_global_fragments,
                              list_of_fragments):
    list_of_global_fragments.compute_maximum_number_of_images()
    video._maximum_number_of_images_in_global_fragments = \
        list_of_global_fragments.maximum_number_of_images
    list_of_fragments.get_accumulable_individual_fragments_identifiers(
        list_of_global_fragments)
    list_of_fragments.get_not_accumulable_individual_fragments_identifiers(
        list_of_global_fragments)
    list_of_fragments.set_fragments_as_accumulable_or_not_accumulable()
    video._has_been_preprocessed = True
    logger.info("Blobs detection and fragmentation finished succesfully.")


def protocols_1_and_2(video, list_of_global_fragments, list_of_fragments):
    video.protocol = 1
    # Accumulation
    print('\nAccumulation 0 ---------------------')
    video.first_accumulation_time = time.time()
    video.accumulation_trial = 0
    video.create_accumulation_folder(iteration_number=0)
    logger.info("Set accumulation network parameters")
    accumulation_network_params = \
        NetworkParams(video.number_of_animals,
                      learning_rate=0.005,
                      keep_prob=1.0,
                      scopes_layers_to_optimize=['fully-connected1',
                                                 'fully_connected_pre_softmax'],
                      save_folder=video._accumulation_folder,
                      image_size=video.identification_image_size)
    logger.info("Starting accumulation")
    list_of_fragments.reset(roll_back_to='fragmentation')
    list_of_global_fragments.reset(roll_back_to='fragmentation')
    if video.tracking_with_knowledge_transfer:
        logger.info("We will restore the network from a \
                    previous model (knowledge transfer): %s"
                    % video.knowledge_transfer_model_folder)
        accumulation_network_params.restore_folder = \
            video.knowledge_transfer_model_folder
    else:
        logger.info("The network will be trained from scratch during accumulation")
        accumulation_network_params.scopes_layers_to_optimize = None
    # instantiate network object
    logger.info("Initialising accumulation network")
    net = ConvNetwork(accumulation_network_params)
    # restore variables from the pretraining
    net.restore()
    # if knowledge transfer is performed on the same animals we don't
    # reinitialise the classification part of the net
    video._identity_transfer = False
    if video.tracking_with_knowledge_transfer:
        net.restore()
        same_animals = getInput("Same animals",
                                "Are you tracking the same animals? y/N")
        if same_animals.lower() == 'n' or same_animals == '':
            net.reinitialize_softmax_and_fully_connected()
        else:
            video._identity_transfer = True
    # instantiate accumulation manager
    logger.info("Initialising accumulation manager")
    # the list of global fragments is ordered in place from the distance
    # (in frames) wrt the core of the first global fragment that will
    # be accumulated
    video._first_frame_first_global_fragment.append(
        list_of_global_fragments.set_first_global_fragment_for_accumulation(
            video, accumulation_trial=0))
    list_of_global_fragments.video = video
    list_of_global_fragments.order_by_distance_to_the_first_global_fragment_for_accumulation(video, accumulation_trial=0)
    accumulation_manager = AccumulationManager(video, list_of_fragments,
                                                list_of_global_fragments,
                                                threshold_acceptable_accumulation=THRESHOLD_ACCEPTABLE_ACCUMULATION)
    # set global epoch counter to 0
    logger.info("Start accumulation")
    global_step = 0
    video._ratio_accumulated_images = accumulate(accumulation_manager,
                                                 video,
                                                 global_step,
                                                 net,
                                                 video.identity_transfer)
    logger.info("Accumulation finished. There are no more acceptable \
                global_fragments for training")
    video._first_accumulation_finished = True
    video.save()
    logger.info("Saving fragments")
    # list_of_fragments.save(video.fragments_path)
    list_of_global_fragments.save(video.global_fragments_path,
                                  list_of_fragments.fragments)
    video.first_accumulation_time = time.time() - video.first_accumulation_time
    list_of_fragments.save_light_list(video._accumulation_folder)
    return net, accumulation_network_params


def assign_after_protocol_1_and_2(video, list_of_global_fragments,
                                  list_of_fragments, net):
    if isinstance(video.first_frame_first_global_fragment, list):
        video.protocol = 1 if video.accumulation_step <= 1 else 2
        video._first_frame_first_global_fragment = \
            video.first_frame_first_global_fragment[video.accumulation_trial]
        list_of_global_fragments.video = video
    video.assignment_time = time.time()
    # Assigner
    print('\nAssignment ----------------')
    assigner(list_of_fragments, video, net)
    video._has_been_assigned = True
    video.assignment_time = time.time() - video.assignment_time
    video.pretraining_time = 0
    video.second_accumulation_time = 0
    video.save()


def protocol_3_pretraining(video, list_of_fragments, list_of_global_fragments):
    print('\nPretraining ---------------------------------------------------------')
    video.pretraining_time = time.time()
    # create folder to store pretraining
    video.create_pretraining_folder()
    # pretraining if first accumulation trial does not cover 90% of the images in global fragments
    pretrain_network_params = NetworkParams(video.number_of_animals,
                                            learning_rate=0.01,
                                            keep_prob=1.0,
                                            use_adam_optimiser=False,
                                            scopes_layers_to_optimize=None,
                                            save_folder=video.pretraining_folder,
                                            image_size=video.identification_image_size)
    list_of_fragments.reset(roll_back_to='fragmentation')
    list_of_global_fragments.order_by_distance_travelled()
    pre_trainer(None, video, list_of_fragments, list_of_global_fragments,
                pretrain_network_params)
    logger.info("Pretraining ended")
    # save changes
    logger.info("Saving changes in video object")
    video._has_been_pretrained = True
    video.save()
    video.pretraining_time = time.time() - video.pretraining_time


def protocol_3_accumulation(video, list_of_global_fragments,
                            list_of_fragments,
                            accumulation_network_params,
                            percentage_of_accumulated_images):
    video.second_accumulation_time = time.time()
    for i in range(1, 4):
        print('\nAccumulation %i ------------------------' %i)
        logger.info("Starting accumulation")
        logger.info("Initialising accumulation manager")
        # Reset used_for_training and acceptable_for_training flags if the old video already had the accumulation done
        list_of_fragments.reset(roll_back_to='fragmentation')
        list_of_global_fragments.reset(roll_back_to='fragmentation')
        video._first_frame_first_global_fragment.append(
            list_of_global_fragments.set_first_global_fragment_for_accumulation(
                video, accumulation_trial=i - 1))
        if video.first_frame_first_global_fragment[-1] is not None:
            # create folder to store accumulation models
            video.create_accumulation_folder(iteration_number=i)
            video.accumulation_trial = i
            logger.info("We will restore the network from a \
                        previous pretraining: %s" % video.pretraining_folder)
            accumulation_network_params.save_folder = video.accumulation_folder
            accumulation_network_params.restore_folder = video.pretraining_folder
            accumulation_network_params.scopes_layers_to_optimize = \
                ['fully-connected1', 'fully_connected_pre_softmax']
            logger.info("Initialising accumulation network")
            net = ConvNetwork(accumulation_network_params)
            # restore variables from the pretraining
            net.restore()
            net.reinitialize_softmax_and_fully_connected()
            # instantiate accumulation manager

            list_of_global_fragments.video = video
            list_of_global_fragments.order_by_distance_to_the_first_global_fragment_for_accumulation(
                video, accumulation_trial=i - 1)
            accumulation_manager = AccumulationManager(video,
                                                       list_of_fragments,
                                                       list_of_global_fragments,
                                                       threshold_acceptable_accumulation = THRESHOLD_ACCEPTABLE_ACCUMULATION)
            # set global epoch counter to 0
            logger.info("Start accumulation")
            global_step = 0
            video._ratio_accumulated_images = accumulate(accumulation_manager,
                                                         video,
                                                         global_step,
                                                         net,
                                                         video.identity_transfer)
            logger.info("Accumulation finished. There are no more \
                        acceptable global_fragments for training")
            percentage_of_accumulated_images.append(video.ratio_accumulated_images)
            list_of_fragments.save_light_list(video._accumulation_folder)
            if video.ratio_accumulated_images > THRESHOLD_ACCEPTABLE_ACCUMULATION:
                break
            else:
                logger.info("This accumulation was not satisfactory. \
                            Try to start from a different global fragment")
        else:
            video._first_frame_first_global_fragment = \
                video.first_frame_first_global_fragment[:-1]

    video.accumulation_trial = np.argmax(percentage_of_accumulated_images)
    video._first_frame_first_global_fragment = \
        video.first_frame_first_global_fragment[video.accumulation_trial]
    video._ratio_accumulated_images = \
        percentage_of_accumulated_images[video.accumulation_trial]
    list_of_global_fragments.video = video
    accumulation_folder_name = 'accumulation_' + str(video.accumulation_trial)
    video._accumulation_folder = os.path.join(video.session_folder,
                                              accumulation_folder_name)
    list_of_fragments.load_light_list(video._accumulation_folder)
    video._second_accumulation_finished = True
    logger.info("Saving global fragments")
    list_of_global_fragments.save(video.global_fragments_path,
                                  list_of_fragments.fragments)
    video.save()
    video.second_accumulation_time = time.time() - video.second_accumulation_time
    return net


def assign_after_protocol_3(video, list_of_fragments, net):
    # Assigner
    video.assignment_time = time.time()
    print('\n---------------------------------------------------------')
    assigner(list_of_fragments, video, net)
    video._has_been_assigned = True
    video.assignment_time = time.time() - video.assignment_time
    video.save()


def save_all_objects(video, list_of_blobs, list_of_fragments,
                     list_of_global_fragments):
    list_of_global_fragments.save(
        video.global_fragments_path,
        list_of_fragments.fragments)
    list_of_fragments.save(video.fragments_path)
    list_of_blobs.update_from_list_of_fragments(
        list_of_fragments.fragments,
        video.fragment_identifier_to_index)
    video.save()


def compute_some_fragments_stats(video, list_of_global_fragments,
                                 list_of_fragments, list_of_blobs):
    video.individual_fragments_stats = \
        list_of_fragments.get_stats(list_of_global_fragments)
    video.compute_overall_P2(list_of_fragments.fragments)
    list_of_fragments.save_light_list(video._accumulation_folder)
    # Update list of blobs
    list_of_blobs.update_from_list_of_fragments(list_of_fragments.fragments,
                                                video.fragment_identifier_to_index)
    list_of_blobs.save(video, path_to_save=video.blobs_path,
                       number_of_chunks=video.number_of_frames)


def create_groundtruth_and_compute_accuracies(video, list_of_blobs):
    groundtruth = \
        generate_groundtruth(video,
                             list_of_blobs.blobs_in_video,
                             start=0,
                             end=video.number_of_frames-1)
    blobs_in_video_groundtruth = groundtruth.blobs_in_video[groundtruth.start:groundtruth.end]
    blobs_in_video = list_of_blobs.blobs_in_video[groundtruth.start:groundtruth.end]
    accuracies, _ = get_accuracy_wrt_groundtruth(video,
                                                 blobs_in_video_groundtruth,
                                                 blobs_in_video,
                                                 video.first_frame_first_global_fragment)
    if accuracies is not None:
        video.gt_start_end = (groundtruth.start,
                              groundtruth.end)
        video.gt_accuracy = accuracies
        video.save()
    return groundtruth
