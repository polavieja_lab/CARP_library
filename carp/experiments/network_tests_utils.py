def check_if_repetition_has_been_computed_network(results_data_frame, job_config, group_size, frames_in_video, frames_in_fragment, repetition):
    rows = results_data_frame.query('test_name == @job_config.test_name' +
                                            ' & CNN_model == @job_config.CNN_model' +
                                            ' & knowledge_transfer_flag == @job_config.knowledge_transfer_flag' +
                                            ' & knowledge_transfer_folder == @job_config.knowledge_transfer_folder' +
                                            ' & pretraining_flag == @job_config.pretraining_flag' +
                                            ' & percentage_of_frames_in_pretaining == @job_config.percentage_of_frames_in_pretaining' +
                                            ' & only_accumulate_one_fragment == @job_config.only_accumulate_one_fragment' +
                                            ' & train_filters_in_accumulation == @job_config.train_filters_in_accumulation' +
                                            ' & accumulation_certainty == @job_config.accumulation_certainty' +
                                            ' & IMDB_codes == @job_config.IMDB_codes' +
                                            ' & ids_codes == @job_config.ids_codes' +
                                            ' & group_size == @group_size' +
                                            ' & frames_in_video == @frames_in_video' +
                                            ' & frames_per_fragment == @frames_in_fragment' +
                                            ' & repetition == @repetition')
    return len(rows) != 0, rows.index


def sample_individuals(dataset, group_size):
    permutation_individual_indices = \
        np.random.permutation(dataset.number_of_animals)
    individual_indices = permutation_individual_indices[:group_size]
    print('individual indices, ', individual_indices)
    return individual_indices


def slice_dataset(images, labels, indicesIndiv):
    ''' Select images and labels relative to a subset of individuals'''
    print('\nSlicing database...')
    images = np.array(np.concatenate([images[labels == ind]
                                      for ind in indicesIndiv], axis=0))
    labels = np.array(np.concatenate([i*np.ones(sum(labels == ind)).astype(int)
                                      for i, ind in enumerate(indicesIndiv)],
                      axis=0))
    print("images shape: ", images.shape)
    print("labels shape: ", labels.shape)
    print("labels: ", np.unique(labels))
    return images, labels


def get_training_and_test_images(images, labels, number_of_images_per_individual,
                          minimum_number_of_images_per_individual,
                          number_test_images_per_individual=300):
    print('\n *** Getting uncorrelated images')
    print("number of images: ", number_of_images_per_individual)
    print("minimum number of images in IMDB: ", minimum_number_of_images_per_individual)
    new_images = []
    new_labels = []
    test_images = []
    test_labels = []
    if number_of_images_per_individual > 15000:  # on average each animal has
                                                # 20000 frames from one camera
                                                # and 20000 frames from the
                                                # other camera. We select only
                                                # 15000 images to get only
                                                # images from one camera
        raise ValueError('The number of images per individual is larger than \
                         the minimum number of images per \
                         individual in the IMDB')

    for i in np.unique(labels):
        # print('individual, ', i)
        # Get images of this individual
        thisIndivImages = images[labels == i][:15000]
        thisIndivLabels = labels[labels == i][:15000]

        perm = np.random.permutation(len(thisIndivImages))
        thisIndivImages = thisIndivImages[perm]
        thisIndivLabels = thisIndivLabels[perm]

        # Get train, validation images and labels
        new_images.append(thisIndivImages[:number_of_images_per_individual])
        new_labels.append(thisIndivLabels[:number_of_images_per_individual])

        # Get test images and labels
        this_test_images = thisIndivImages[number_of_images_per_individual:
                                           number_of_images_per_individual +
                                           number_test_images_per_individual]
        this_test_labels = thisIndivLabels[number_of_images_per_individual:
                                           number_of_images_per_individual +
                                           number_test_images_per_individual]
        test_images.append(this_test_images)
        test_labels.append(this_test_labels)

    images = np.concatenate(new_images, axis=0)
    labels = np.concatenate(new_labels, axis=0)
    test_images = np.concatenate(test_images, axis=0)
    test_labels = np.concatenate(test_labels, axis=0)
    print("train and val images shape: ", images.shape)
    print("train and val labels shape: ", labels.shape)
    print("test images shape: ", test_images.shape)
    print("test labels shape: ", test_labels.shape)
    return images, labels, test_images, test_labels


def standarize_images(images):
    mean_images = np.expand_dims(np.expand_dims(np.mean(images, axis=(1, 2)),
                                                axis=1),
                                 axis=2)
    std_images = np.expand_dims(np.expand_dims(np.std(images, axis=(1, 2)),
                                               axis=1),
                                axis=2)
    return ((images - mean_images) / std_images).astype('float32')


def correct_loss_nan(store_validation_accuracy_and_loss_data):
    if np.isnan(store_validation_accuracy_and_loss_data.loss[-1]):
        store_validation_accuracy_and_loss_data.loss = \
            store_validation_accuracy_and_loss_data.loss[:-1]
        store_validation_accuracy_and_loss_data.accuracy = \
            store_validation_accuracy_and_loss_data.accuracy[:-1]
        store_validation_accuracy_and_loss_data.individual_accuracy = \
            store_validation_accuracy_and_loss_data.individual_accuracy[:-1]
    return store_validation_accuracy_and_loss_data


def compute_test_accuracies(test_labels, predictions):
    correct_assignment_per_identity = [0] * len(np.unique(test_labels))
    number_of_labels_per_identity = [0] * len(np.unique(test_labels))
    for test_label, prediction in zip(test_labels, predictions):
        correct_assignment_per_identity[test_label] += (test_label + 1) == \
                                                        prediction
        number_of_labels_per_identity[test_label] += 1

    individual_accuracy = np.asarray(correct_assignment_per_identity) / \
        np.asarray(number_of_labels_per_identity)
    accuracy = np.sum(correct_assignment_per_identity) / \
        np.sum(number_of_labels_per_identity)
    print("test individual accuracy, ", individual_accuracy)
    print("test accuracy ", accuracy)
    return individual_accuracy, accuracy
