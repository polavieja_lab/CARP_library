from __future__ import absolute_import, division, print_function
import os


class TestConfig(object):
    def __init__(self, save_folder=None, cluster=None, test_dictionary=None):
        self.cluster = int(cluster)
        self.save_folder = save_folder
        for key in test_dictionary:
            setattr(self, key, test_dictionary[key])

    def create_folders_structure(self):
        #create main condition folder
        print(self.save_folder)
        self.condition_path = os.path.join(self.save_folder, 'library_test_' + self.test_name)
        if not os.path.exists(self.condition_path):
            os.makedirs(self.condition_path)
        #create subfolders for group sizes
        for group_size in self.group_sizes:
            group_size_path = os.path.join(self.condition_path,'group_size_' + str(group_size))
            if not os.path.exists(group_size_path):
                os.makedirs(group_size_path)
            #create subfolders for frames_in_video
            for frames_in_video in self.frames_in_video:
                num_frames_path = os.path.join(group_size_path,'num_frames_' + str(frames_in_video))
                if not os.path.exists(num_frames_path):
                    os.makedirs(num_frames_path)
                #create subfolders for frames_in_fragment or scale and shape parameter from gamma function
                if "algorithm" in self.test_name:
                    for scale_parameter in self.scale_parameter:
                        scale_parameter_path = os.path.join(num_frames_path, 'scale_parameter_' + str(scale_parameter))
                        if not os.path.exists(scale_parameter_path):
                            os.makedirs(scale_parameter_path)
                        for shape_parameter in self.shape_parameter:
                            shape_parameter_path = os.path.join(scale_parameter_path, 'shape_parameter_' + str(shape_parameter))
                            if not os.path.exists(shape_parameter_path):
                                os.makedirs(shape_parameter_path)
                            for repetition in self.repetitions:
                                repetition_path = os.path.join(shape_parameter_path, 'repetition_' + str(repetition))
                                if not os.path.exists(repetition_path):
                                    os.makedirs(repetition_path)
                elif "uncorrelated" in self.test_name:
                    for frames_in_fragment in self.frames_per_individual_fragment:
                        frames_in_fragment_path = os.path.join(num_frames_path, 'frames_in_fragment_' + str(frames_in_fragment))
                        if not os.path.exists(frames_in_fragment_path):
                            os.makedirs(frames_in_fragment_path)
                        for repetition in self.repetitions:
                            repetition_path = os.path.join(frames_in_fragment_path, 'repetition_' + str(repetition))
                            if not os.path.exists(repetition_path):
                                os.makedirs(repetition_path)
#
#
# """ networks experiments helpers """
#
#
# def check_if_repetition_has_been_computed_network(results_data_frame, job_config, group_size, frames_in_video, frames_in_fragment, repetition):
#     rows = results_data_frame.query('test_name == @job_config.test_name' +
#                                             ' & CNN_model == @job_config.CNN_model' +
#                                             ' & knowledge_transfer_flag == @job_config.knowledge_transfer_flag' +
#                                             ' & knowledge_transfer_folder == @job_config.knowledge_transfer_folder' +
#                                             ' & pretraining_flag == @job_config.pretraining_flag' +
#                                             ' & percentage_of_frames_in_pretaining == @job_config.percentage_of_frames_in_pretaining' +
#                                             ' & only_accumulate_one_fragment == @job_config.only_accumulate_one_fragment' +
#                                             ' & train_filters_in_accumulation == @job_config.train_filters_in_accumulation' +
#                                             ' & accumulation_certainty == @job_config.accumulation_certainty' +
#                                             ' & IMDB_codes == @job_config.IMDB_codes' +
#                                             ' & ids_codes == @job_config.ids_codes' +
#                                             ' & group_size == @group_size' +
#                                             ' & frames_in_video == @frames_in_video' +
#                                             ' & frames_per_fragment == @frames_in_fragment' +
#                                             ' & repetition == @repetition')
#     return len(rows) != 0, rows.index
#
#
# def sample_individuals(dataset, group_size):
#     permutation_individual_indices = \
#         np.random.permutation(dataset.number_of_animals)
#     individual_indices = permutation_individual_indices[:group_size]
#     print('individual indices, ', individual_indices)
#     return individual_indices
#
#
# def slice_dataset(images, labels, indicesIndiv):
#     ''' Select images and labels relative to a subset of individuals'''
#     print('\nSlicing database...')
#     images = np.array(np.concatenate([images[labels == ind]
#                                       for ind in indicesIndiv], axis=0))
#     labels = np.array(np.concatenate([i*np.ones(sum(labels == ind)).astype(int)
#                                       for i, ind in enumerate(indicesIndiv)],
#                       axis=0))
#     print("images shape: ", images.shape)
#     print("labels shape: ", labels.shape)
#     print("labels: ", np.unique(labels))
#     return images, labels
#
#
# def get_training_and_test_images(images, labels, number_of_images_per_individual,
#                           minimum_number_of_images_per_individual,
#                           number_test_images_per_individual=300):
#     print('\n *** Getting uncorrelated images')
#     print("number of images: ", number_of_images_per_individual)
#     print("minimum number of images in IMDB: ", minimum_number_of_images_per_individual)
#     new_images = []
#     new_labels = []
#     test_images = []
#     test_labels = []
#     if number_of_images_per_individual > 15000:  # on average each animal has
#                                                 # 20000 frames from one camera
#                                                 # and 20000 frames from the
#                                                 # other camera. We select only
#                                                 # 15000 images to get only
#                                                 # images from one camera
#         raise ValueError('The number of images per individual is larger than \
#                          the minimum number of images per \
#                          individual in the IMDB')
#
#     for i in np.unique(labels):
#         # print('individual, ', i)
#         # Get images of this individual
#         thisIndivImages = images[labels == i][:15000]
#         thisIndivLabels = labels[labels == i][:15000]
#
#         perm = np.random.permutation(len(thisIndivImages))
#         thisIndivImages = thisIndivImages[perm]
#         thisIndivLabels = thisIndivLabels[perm]
#
#         # Get train, validation images and labels
#         new_images.append(thisIndivImages[:number_of_images_per_individual])
#         new_labels.append(thisIndivLabels[:number_of_images_per_individual])
#
#         # Get test images and labels
#         this_test_images = thisIndivImages[number_of_images_per_individual:
#                                            number_of_images_per_individual +
#                                            number_test_images_per_individual]
#         this_test_labels = thisIndivLabels[number_of_images_per_individual:
#                                            number_of_images_per_individual +
#                                            number_test_images_per_individual]
#         test_images.append(this_test_images)
#         test_labels.append(this_test_labels)
#
#     images = np.concatenate(new_images, axis=0)
#     labels = np.concatenate(new_labels, axis=0)
#     test_images = np.concatenate(test_images, axis=0)
#     test_labels = np.concatenate(test_labels, axis=0)
#     print("train and val images shape: ", images.shape)
#     print("train and val labels shape: ", labels.shape)
#     print("test images shape: ", test_images.shape)
#     print("test labels shape: ", test_labels.shape)
#     return images, labels, test_images, test_labels
#
#
# def standarize_images(images):
#     mean_images = np.expand_dims(np.expand_dims(np.mean(images, axis=(1, 2)),
#                                                 axis=1),
#                                  axis=2)
#     std_images = np.expand_dims(np.expand_dims(np.std(images, axis=(1, 2)),
#                                                axis=1),
#                                 axis=2)
#     return ((images - mean_images) / std_images).astype('float32')
#
#
# def correct_loss_nan(store_validation_accuracy_and_loss_data):
#     if np.isnan(store_validation_accuracy_and_loss_data.loss[-1]):
#         store_validation_accuracy_and_loss_data.loss = \
#             store_validation_accuracy_and_loss_data.loss[:-1]
#         store_validation_accuracy_and_loss_data.accuracy = \
#             store_validation_accuracy_and_loss_data.accuracy[:-1]
#         store_validation_accuracy_and_loss_data.individual_accuracy = \
#             store_validation_accuracy_and_loss_data.individual_accuracy[:-1]
#     return store_validation_accuracy_and_loss_data
#
#
# def compute_test_accuracies(test_labels, predictions):
#     correct_assignment_per_identity = [0] * len(np.unique(test_labels))
#     number_of_labels_per_identity = [0] * len(np.unique(test_labels))
#     for test_label, prediction in zip(test_labels, predictions):
#         correct_assignment_per_identity[test_label] += (test_label + 1) == \
#                                                         prediction
#         number_of_labels_per_identity[test_label] += 1
#
#     individual_accuracy = np.asarray(correct_assignment_per_identity) / \
#         np.asarray(number_of_labels_per_identity)
#     accuracy = np.sum(correct_assignment_per_identity) / \
#         np.sum(number_of_labels_per_identity)
#     print("test individual accuracy, ", individual_accuracy)
#     print("test accuracy ", accuracy)
#     return individual_accuracy, accuracy
#
#
# """ generate blob lists """
#
#
# def check_if_repetition_has_been_computed(results_data_frame, job_config, group_size, frames_in_video, scale_parameter, shape_parameter, repetition):
#
#     return len(results_data_frame.query('test_name == @job_config.test_name' +
#                                             ' & CNN_model == @job_config.CNN_model' +
#                                             ' & knowledge_transfer_flag == @job_config.knowledge_transfer_flag' +
#                                             ' & knowledge_transfer_folder == @job_config.knowledge_transfer_folder' +
#                                             ' & IMDB_codes == @job_config.IMDB_codes' +
#                                             ' & ids_codes == @job_config.ids_codes' +
#                                             ' & group_size == @group_size' +
#                                             ' & frames_in_video == @frames_in_video' +
#                                             ' & scale_parameter == @scale_parameter' +
#                                             ' & shape_parameter == @shape_parameter'
#                                             ' & repetition == @repetition')) != 0
#
#
# class BlobsListConfig(object):
#     def __init__(self,
#                     number_of_animals = None,
#                     scale_parameter = None,
#                     shape_parameter = None,
#                     number_of_frames = None,
#                     repetition = None):
#         self.number_of_animals = number_of_animals
#         self.scale_parameter = scale_parameter
#         self.shape_parameter = shape_parameter
#         self.max_number_of_frames_per_fragment = number_of_frames
#         self.min_number_of_frames_per_fragment = 1
#         self.number_of_frames = number_of_frames
#         self.repetition = repetition
#         self.IMDB_codes = []
#         self.ids_codes = []
#
#
# def subsample_dataset_by_individuals(dataset, config):
#     # We need to consider that for every individual fragment there are two frames that are not considered for training
#     # In order to have the correct number of trainable images per individual fragment we need to increase the number of
#     # frames in the video to account for those two frames. The total number of frames needes is:
#     # number_of_frames = int(config.number_of_frames + 2 * config.number_of_frames / config.number_of_frames_per_fragment)
#     number_of_frames = config.number_of_frames
#     if config.number_of_animals > dataset.number_of_animals:
#         raise ValueError("The number of animals for subsampling (%i) cannot be bigger than the number of animals in the dataset (%i)" %(config.number_of_animals, dataset.number_of_animals))
#
#     if number_of_frames > dataset.minimum_number_of_images_per_animal:
#         raise ValueError("The number of frames for subsampling (%i) cannot be bigger than the minimum number of images per animal in the dataset (%i)" %(number_of_frames, dataset.minimum_number_of_images_per_animal))
#
#     # copy dataset specifics to config. This allows to restore the dataset if needed
#     config.IMDB_codes = dataset.IMDB_codes
#     config.ids_codes = dataset.ids_codes
#     # set permutation of individuals
#     np.random.seed(config.repetition)
#     config.identities = dataset.identities[np.random.permutation(config.number_of_animals)]
#     print("identities, ", config.identities)
#     # set stating frame
#     # we set the starting frame so that we take images from both videos of the library.
#     if dataset.minimum_number_of_images_per_animal > 20000:
#         # This is the code used when the part A and B of CARP were together
#         config.starting_frame = np.random.randint(int(dataset.minimum_number_of_images_per_animal/3)-number_of_frames)
#     else:
#         config.starting_frame = np.random.randint(int(dataset.minimum_number_of_images_per_animal)-number_of_frames)
#
#     print("starting frame, ", config.starting_frame)
#
#     subsampled_images = []
#     subsampled_centroids = []
#     for identity in config.identities:
#         indices_identity = np.where(dataset.labels == identity)[0]
#         subsampled_images.append(np.expand_dims(dataset.images[indices_identity][config.starting_frame:config.starting_frame + number_of_frames], axis = 1))
#         subsampled_centroids.append(np.expand_dims(dataset.centroids[indices_identity][config.starting_frame:config.starting_frame + number_of_frames], axis = 1))
#
#     return np.concatenate(subsampled_images, axis = 1), np.concatenate(subsampled_centroids, axis = 1)
#
#
# def get_next_number_of_blobs_in_fragment(config):
#     scale = config.scale_parameter
#     shape = config.shape_parameter
#     X = gamma(a=shape, loc=1, scale=scale)
#     number_of_frames_per_fragment = int(X.rvs(1))
#     while number_of_frames_per_fragment < config.min_number_of_frames_per_fragment or number_of_frames_per_fragment > config.max_number_of_frames_per_fragment:
#         number_of_frames_per_fragment = int(X.rvs(1))
#
#     return number_of_frames_per_fragment
#
#
# def generate_list_of_blobs(identification_images, centroids, config):
#
#     blobs_in_video = []
#     number_of_fragments = 0
#
#     print("\n***********Generating list of blobs")
#     print("centroids shape ", centroids.shape)
#     print("identification_images shape", identification_images.shape)
#     for identity in range(config.number_of_animals):
#         # decide length of first individual fragment for this identity
#         number_of_blobs_per_fragment = \
#             get_next_number_of_blobs_in_fragment(config)
#         number_of_fragments += 1
#         number_of_frames_per_crossing_fragment = 3
#         blobs_in_fragment = 0
#         blobs_in_crossing_fragment = 0
#         blobs_in_identity = []
#         for frame_number in range(config.number_of_frames):
#             centroid = centroids[frame_number,identity,:]
#             image = identification_images[frame_number,identity,:,:]
#
#             blob = Blob(centroid, None, None, None,
#                         number_of_animals = config.number_of_animals)
#             blob.frame_number = frame_number
#
#             if blobs_in_fragment <= number_of_blobs_per_fragment:
#                 blob._is_an_individual = True
#                 blob._is_a_crossing = False
#                 blob._image_for_identification = \
#                     ((image - np.mean(image))/np.std(image)).astype("float16")
#                 blob._user_generated_identity = identity + 1
#                 if blobs_in_fragment != 0:
#                     blob.previous = [blobs_in_identity[frame_number-1]]
#                     blobs_in_identity[frame_number-1].next = [blob]
#                 blobs_in_fragment += 1
#             elif blobs_in_crossing_fragment < number_of_frames_per_crossing_fragment:
#                 blobs_in_crossing_fragment += 1
#                 blob = None
#             else:
#                 blob._is_an_individual = True
#                 blob._is_a_crossing = False
#                 blob._image_for_identification = \
#                     ((image - np.mean(image))/np.std(image)).astype("float16")
#                 blob._user_generated_identity = identity + 1
#                 blob.previous = []
#
#                 blobs_in_fragment = 1
#                 blobs_in_crossing_fragment = 0
#                 number_of_fragments += 1
#                 number_of_blobs_per_fragment = \
#                     get_next_number_of_blobs_in_fragment(config)
#             blobs_in_identity.append(blob)
#         blobs_in_video.append(blobs_in_identity)
#
#     blobs_in_video = zip(*blobs_in_video)
#     blobs_in_video = [[b for b in blobs_in_frame if b is not None] for blobs_in_frame in blobs_in_video]
#     print("number of fragments ", number_of_fragments)
#     return blobs_in_video
