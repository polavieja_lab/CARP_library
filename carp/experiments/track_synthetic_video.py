from __future__ import absolute_import, division, print_function
from carp.experiments.idtrackerai_steps import init_video,\
                                                preprocessing,\
                                                extra_preprocessing_steps,\
                                                protocols_1_and_2,\
                                                assign_after_protocol_1_and_2,\
                                                protocol_3_pretraining,\
                                                protocol_3_accumulation,\
                                                assign_after_protocol_3,\
                                                save_all_objects,\
                                                compute_some_fragments_stats,\
                                                create_groundtruth_and_compute_accuracies
from idtrackerai.constants import THRESHOLD_ACCEPTABLE_ACCUMULATION


def track_synthetic_video(video_folder, group_size, scale_parameter,
                          shape_parameter, repetition, dataset,
                          frames_in_video, starting_frame=None,
                          knowledge_transfer_flag=None,
                          knowledge_transfer_folder=None):
    # Init video
    imsize = dataset.images.shape[1]
    video = init_video(video_folder, imsize, group_size, frames_in_video,
                       knowledge_transfer_flag, knowledge_transfer_folder)
    # Preprocessing (create synthetic video)
    video, list_of_blobs, list_of_fragments,\
        list_of_global_fragments, list_of_blobs_config =\
        preprocessing(video, group_size,
                      scale_parameter, shape_parameter,
                      frames_in_video, repetition,
                      dataset, starting_frame)
    # Only continues if there are global fragments
    if len(list_of_global_fragments.global_fragments) > 0:
        # Extra steps and compute fragments statistics
        extra_preprocessing_steps(video,
                                  list_of_global_fragments,
                                  list_of_fragments)
        # Protocol cascade starts
        net, accumulation_network_params =\
            protocols_1_and_2(video,
                              list_of_global_fragments,
                              list_of_fragments)
        # If the ratio of accumulated images is enough we assign
        if video.ratio_accumulated_images > THRESHOLD_ACCEPTABLE_ACCUMULATION:
            assign_after_protocol_1_and_2(video,
                                          list_of_global_fragments,
                                          list_of_fragments,
                                          net)
        # Else we move to protocol 3
        else:
            video.protocol = 3
            percentage_of_accumulated_images = \
                [video._ratio_accumulated_images]
            # Pretraining the network
            protocol_3_pretraining(video,
                                   list_of_fragments,
                                   list_of_global_fragments)
            # Accumulation
            net = protocol_3_accumulation(video,
                                          list_of_global_fragments,
                                          list_of_fragments,
                                          accumulation_network_params,
                                          percentage_of_accumulated_images)
            # Assignment
            assign_after_protocol_3(video, list_of_fragments, net)

        # finish and save
        save_all_objects(video, list_of_blobs,
                         list_of_fragments,
                         list_of_global_fragments)

        # Compute fragments stats
        compute_some_fragments_stats(video, list_of_global_fragments,
                                     list_of_fragments, list_of_blobs)

        # Generate groundtruth
        groundtruth = \
            create_groundtruth_and_compute_accuracies(video, list_of_blobs)

        video.total_time = sum([video.assignment_time,
                                video.pretraining_time,
                                video.second_accumulation_time,
                                video.assignment_time,
                                video.first_accumulation_time])

        return True, video, list_of_blobs, list_of_fragments, \
            list_of_global_fragments, groundtruth, list_of_blobs_config
    else:
        print('No global fragments in synthetic video')
        video.save()
        return False, video, list_of_blobs, list_of_fragments, \
            list_of_global_fragments, None, list_of_blobs_config
