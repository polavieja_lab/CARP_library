from __future__ import absolute_import, division, print_function
import os
import sys
import numpy as np
import h5py


IMDBsDict = {
            'A': os.path.join('CARP','IMDBs','IMDBs_portraits_new','TU20170131_31dpf_40indiv_34770ImPerInd_portraits_0.hdf5'),
            'B': os.path.join('CARP','IMDBs','IMDBs_portraits_new','TU20170201_31pdf_72indiv_38739ImPerInd_portraits_0.hdf5'),
            'C': os.path.join('CARP','IMDBs','IMDBs_portraits_new','TU20170202_31pdf_72indiv_38913ImPerInd_portraits_0.hdf5'),
            'D': os.path.join('CARP','IMDBs','IMDBs_bodies_new','TU20170131_31dpf_40indiv_34770ImPerInd_bodies_0.hdf5'),
            'E': os.path.join('CARP','IMDBs','IMDBs_bodies_new','TU20170201_31pdf_72indiv_38739ImPerInd_bodies_0.hdf5'),
            'F': os.path.join('CARP','IMDBs','IMDBs_bodies_new','TU20170202_31pdf_72indiv_38913ImPerInd_bodies_0.hdf5'),
            'G': os.path.join('CARP','IMDBs','IMDBs_bodyblobs_new','TU20170131_31dpf_40indiv_34770ImPerInd_bodyblobs_0.hdf5'),
            'H': os.path.join('CARP','IMDBs','IMDBs_bodyblobs_new','TU20170201_31pdf_72indiv_38739ImPerInd_bodyblobs_0.hdf5'),
            'I': os.path.join('CARP','IMDBs','IMDBs_bodyblobs_new','TU20170202_31pdf_72indiv_38913ImPerInd_bodyblobs_0.hdf5'),
            'J': os.path.join('CARP','IMDBs','preprocessed','TU20170131_40individuals_preprocessed_a_0.hdf5'),
            'K': os.path.join('CARP','IMDBs','preprocessed','TU20170201_72individuals_preprocessed_a_0.hdf5'),
            'L': os.path.join('CARP','IMDBs','preprocessed','TU20170202_72individuals_preprocessed_a_0.hdf5'),
            'M': os.path.join('CARP','IMDBs','preprocessed','TU20170131_40individuals_preprocessed_b_0.hdf5'),
            'N': os.path.join('CARP','IMDBs','preprocessed','TU20170201_72individuals_preprocessed_b_0.hdf5'),
            'O': os.path.join('CARP','IMDBs','preprocessed','TU20170202_72individuals_preprocessed_b_0.hdf5'),
            }


class Dataset(object):
    def __init__(self, IMDB_codes='A', ids_codes='a', cluster=0, data_folder=None):
        self.IMDB_codes = IMDB_codes
        self.ids_codes = ids_codes
        self.cluster = cluster
        # Get list of IMDBPaths form IMDB_codes
        print('\nReading IMDB_codes and ids_codes...')
        if not int(self.cluster):
            self.datafolder = os.getenv("HOME") if data_folder is None \
                                                    else data_folder
        elif int(self.cluster):
            self.datafolder = '/admin/'

        self.IMDBPaths = [os.path.join(self.datafolder, IMDBsDict[code])
                          for code in self.IMDB_codes]

    def get_ids_in_IMDB(self, id_code, numIndivIMDB):
        if id_code == 'a':  # all ids
            return range(numIndivIMDB)
        elif id_code == 'f':  # first half idsInIMDBs
            return range(int(numIndivIMDB/2))
        elif id_code == 's':  # first half idsInIMDBs
            return range(int(numIndivIMDB/2), numIndivIMDB)

    def loadIMDBs(self):
        # Initialize variables
        self.images = []
        self.labels = []
        self.centroids = []
        self.number_of_animals = 0
        for (IMDBPath, id_code) in zip(self.IMDBPaths, self.ids_codes):
            # Load IMDB
            _, imagesIMDB, labelsIMDB, centroidsIMDB, _, numIndivIMDB, _ =\
                loadIMDB(IMDBPath)
            idsInIMDB = self.get_ids_in_IMDB(id_code, numIndivIMDB)
            # If the number of individuals requested is smaller
            # we slice the IMDB
            if numIndivIMDB > len(idsInIMDB):
                imagesIMDB, labelsIMDB, centroidsIMDB = \
                    sliceDatabase(imagesIMDB, labelsIMDB,
                                  centroidsIMDB, idsInIMDB)
            # Update labels values according to the number of
            # individuals already loaded
            labelsIMDB = labelsIMDB + self.number_of_animals
            # Append labels and images to the list
            self.images.append(imagesIMDB)
            self.labels.append(labelsIMDB)
            self.centroids.append(centroidsIMDB)
            # Update number of individuals loaded
            self.number_of_animals += len(idsInIMDB)
            # To clear memory
            imagesIMDB = None
            labelsIMDB = None
            centroidsIMDB = None
        # Stack all images and labes
        self.images = np.concatenate(self.images, axis=0)
        self.labels = np.concatenate(self.labels, axis=0)
        self.centroids = np.concatenate(self.centroids, axis=0)
        self.minimum_number_of_images_per_animal = \
            np.min([np.sum(self.labels == i) for i in np.unique(self.labels)])
        self.identities = np.unique(self.labels)


def loadIMDB(IMDBPath):
    # check if the train database exists, and load it!
    IMDBname = getIMDBNameFromPath(IMDBPath)
    IMDBdir = os.path.dirname(IMDBPath)
    print('\nloading %s...' % IMDBname)
    # checkDatabase(IMDBname)
    with h5py.File(IMDBdir + "/" + IMDBname + '_%i.hdf5', 'r', driver='family') as databaseTrain:
        IMDB_info, images, labels, centroids, areas = \
            getVarAttrFromHdf5(databaseTrain)
        number_of_individual_in_IMDB,\
            minimum_number_of_images_per_individual =\
            getAttrsFromGroup(IMDB_info, ['numIndiv', 'numImagesPerIndiv'])
        minimum_number_of_images_per_individual = int(minimum_number_of_images_per_individual)
        print([item for item in IMDB_info.attrs.items()])
    print('\ndatabase %s loaded' % IMDBname)
    return IMDB_info, images, labels, centroids, areas, number_of_individual_in_IMDB, minimum_number_of_images_per_individual


def sliceDatabase(images, labels, centroids, individual_indices):
    ''' Select images and labels relative to a subset of individuals'''
    print('\nSlicing database...')
    sliced_images = [image for indiv_images in [images[np.where(labels == ind)[0]] for ind in individual_indices] for image in indiv_images]
    sliced_labels = [label for indiv_labels in [i*np.ones(len(np.where(labels == ind)[0])).astype(int) for i,ind in enumerate(individual_indices)] for label in indiv_labels]
    sliced_centroids = [centroid for indiv_centroids in [centroids[np.where(labels == ind)[0]] for ind in individual_indices] for centroid in indiv_centroids]
    return np.asarray(sliced_images), np.asarray(sliced_labels), np.asarray(sliced_centroids)


def getVarAttrFromHdf5(database):
    # collect the info
    grp = database['database']
    datanames = grp.keys()
    labels = grp['labels'][()]
    try:
        images = grp['images'][()]
    except:
        if 'bodyblobs' in datanames:
            images = grp['bodyblobs'][()]
        elif 'bodies' in datanames:
            images = grp['bodies'][()]
        elif 'portraits' in datanames:
            images = grp['portraits'][()]
    centroids = grp['centroids'][()]
    areas = grp['areas'][()]
    # info = [item for item in grp.attrs.iteritems()]
    return grp, images, labels, centroids, areas


def getAttrsFromGroup(grp, variables):
    # retrieve an array from a h5py file
    return [grp.attrs[var] for var in variables]


def getIMDBNameFromPath(IMDBPath):
    filename, extension = os.path.splitext(IMDBPath)
    IMDBName = '_'.join(filename.split('/')[-1].split('_')[:-1])
    return IMDBName


def getIMDBInfoFromName(IMDBName):
    strain = IMDBName.split('_')[0]
    age = IMDBName.split('_')[1]
    numIndiv = int(IMDBName.split('_')[2][:-5])
    numImPerIndiv = int(IMDBName.split('_')[3][:-8])
    return strain, age, numIndiv, numImPerIndiv


if __name__ == '__main__':
    from matplotlib import pyplot as plt
    plt.ion()
    dataset = Dataset(IMDB_codes=sys.argv[1], ids_codes=sys.argv[2],
                      cluster=0, data_folder=sys.argv[3])
    dataset.loadIMDBs()

    fig1, ax = plt.subplots(1, 1)
    fig2, ax_arr = plt.subplots(dataset.number_of_animals//4, 4)
    for i, ax2 in zip(range(dataset.number_of_animals), ax_arr.flatten()):
        this_centroids = dataset.centroids[dataset.labels == i]
        ax.scatter(this_centroids[:, 0], this_centroids[:, 1] +
                   750*(int(i/8)), alpha=.01, s=5)
        ax.text(np.mean(this_centroids[:, 0]), np.mean(this_centroids[:, 1]) +
                750*(int(i/8)), str(i))
        ax.axis('equal')
        image_index = np.where(dataset.labels == i)[0][0]
        ax2.imshow(dataset.images[image_index, ...], cmap='gray')
        ax2.set_yticklabels([]), ax2.set_xticklabels([])
        ax2.set_title('label %i' % i)

    plt.figure()
    plt.imshow(dataset.images[0, ...], cmap='gray')
