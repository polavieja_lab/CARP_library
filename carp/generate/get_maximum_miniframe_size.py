import os
import glob
import numpy as np
import pandas as pd

from GUI_utils_library import selectDir
from build_library_utils import retrieveInfoLib

if __name__ == '__main__':
    libPath = selectDir('./')
    ageInDpf, subDirs, strain = retrieveInfoLib(libPath)
    print(subDirs)
    max_size = 0
    for i, subDir in enumerate(subDirs):
        path = libPath + '/' + subDir
        preprocessing_folder = os.path.join(path, 'preprocessing', 'segmentation')
        print(preprocessing_folder)
        segm_files = glob.glob(preprocessing_folder + '/*.pkl')
        print(segm_files)
        for segm in segm_files:
            segm_dataframe = pd.read_pickle(segm)
            for miniframes_in_frames in segm_dataframe.miniFrames:
                for miniframe in miniframes_in_frames:
                    if np.max(miniframe.shape) > max_size:
                        max_size = np.max(miniframe.shape)
