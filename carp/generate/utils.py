from __future__ import absolute_import, division, print_function
import os
from joblib import Parallel, delayed
import multiprocessing
from natsort import natsorted, ns
import scipy.spatial.distance as scisd
import pandas as pd
import numpy as np
from carp.generate.py_utils import loadFile, saveFile, flatten


def retrieveInfoLib(libPath):
    #the folder's name is the age of the individuals
    ageInDpf = os.path.split(libPath)[-1]
    strain = libPath.split('/')[-2]

    # get the list of subfolders
    subDirs = [d for d in os.listdir(libPath) if os.path.isdir(libPath +'/'+ d)]
    subDirs = [subDir for subDir in subDirs if 'group' in subDir or 'Group' in subDir and 'prep' not in subDir]
    subDirs = natsorted(subDirs, alg=ns.IGNORECASE)

    return ageInDpf, subDirs, strain


def portraitsToIMDB(identities, images, centroids, areas, numAnimalsInGroup, groupNum):
    labels = np.asarray(flatten([ids for ids in identities if len(ids) == numAnimalsInGroup])) + numAnimalsInGroup*groupNum
    images = np.asarray(flatten([imgs for imgs in images if len(imgs) == numAnimalsInGroup]))
    centroids = np.asarray(flatten([ctns for ctns in centroids if len(ctns) == numAnimalsInGroup]))
    areas = np.asarray(flatten([ars for ars in areas if len(ars) == numAnimalsInGroup]))

    if len(images) != len(labels):
        raise ValueError('The number of portraits and labels should match.')
    print('Group, ', groupNum)
    print('Labels, ', labels)

    return labels, images, centroids, areas

def orderCenters(centers, video, transform):
    """
    orders points according to the video number and the transformation ('rotation', 'translation' or 'none')
    this is supposed to work for the following libraries in the following way:
    TU20160413 and TU20160428:
        transform = 'rotation' (180 degrees)
        video1:             video2:
            0 3                 2 1
            1 2                 3 0
    TU20160920
        video1:
            1 0
            2 3
    TU20170131, TU20170201 and TU20170202:
        video1:             video2:
            1 0     7 6         7 6     1 0
            2 3     4 5         4 5     2 3
    Assuming that (0,0) is in the top-left corner because the centers come from the image
    """
    def shift(seq, n):
        n = n % len(seq)
        return seq[n:] + seq[:n]
    def rearrange(cents):
        newCents = []
        newCents.append(cents[6])
        newCents.append(cents[7])
        newCents.append(cents[4])
        newCents.append(cents[5])
        newCents.append(cents[2])
        newCents.append(cents[3])
        newCents.append(cents[0])
        newCents.append(cents[1])
        return np.asarray(newCents)
    #select the circle with minimal x (at most the arena can be 44deg rotated wrt its center)
    cents = np.asarray(centers)
    #centroid of the centers
    centroid = np.true_divide(np.sum(cents, axis=0), cents.shape[0])
    #put the centroid in the origin
    trCenters = np.subtract(cents, centroid)
    #compute the arctan to order
    arctans = [np.arctan2(s[0],s[1]) for s in trCenters]
    cents = cents[np.argsort(arctans)]
    cents = list(tuple(map(tuple,cents)))
    if video == 2 and transform == 'rotation':
        cents = shift(cents, 2)
    if video == 2 and transform == 'translation':
        cents = rearrange(cents)
    return cents

def assignCenterFrame(centers,centroids,video, transform):
    """
    centers: centers of the arenas
    centroids: centroids of the fish for a frame
    """
    centers = orderCenters(centers,video,transform) # Order the centers counterclockwise
    d = scisd.cdist(centers,centroids) # Compute the distance of each fish to each centroid
    identities = np.argmin(d,axis=0) # Assign identity by the centroid to which they are closer
    return identities #, centers

def assignCenterAndSave(path,centers, video, transform):
    df, numSegment = loadFile(path, 'segmentation')
    dfPermutations = pd.DataFrame(index=df.index,columns={'permutations'})
    frameIndices = loadFile(path, 'frameIndices')
    segmentIndices = frameIndices.loc[frameIndices.loc[:,'segment']==int(numSegment)]
    segmentIndices = segmentIndices.index.tolist()
    dfGlobal = pd.DataFrame(index = segmentIndices,columns={'identities','permutations','centroids','areas'})
    for i, (centroids, index) in enumerate(zip(df.centroids,df.index)):
        dfPermutations.loc[index,'permutations'] = assignCenterFrame(centers,centroids,video,transform)
        dfGlobal.loc[segmentIndices[i],'identities'] = dfPermutations.loc[index,'permutations']
        dfGlobal.loc[segmentIndices[i],'permutations'] = dfPermutations.loc[index,'permutations']
        dfGlobal.loc[segmentIndices[i],'centroids'] = centroids
        dfGlobal.loc[segmentIndices[i],'areas'] = df.loc[index,'areas']

    df['permutations'] = dfPermutations['permutations']
    saveFile(path, df, 'segment')
    return dfGlobal

def assignCenters(paths,centers,video = 1,transform = 'none'):
    num_cores = multiprocessing.cpu_count()
    # num_cores = 1

    outDf = Parallel(n_jobs=num_cores)(delayed(assignCenterAndSave)(path, centers, video, transform) for path in paths)

    dfGlobal = pd.concat(outDf)
    dfGlobal = dfGlobal.sort_index(axis=0,ascending=True)
    return dfGlobal



# def portraitsToIMDB(portraits_df, numAnimalsInGroup, groupNum):
#     labels = np.asarray(flatten([perm for perm in portraits_df.loc[:,'permutations'] if len(perm) == numAnimalsInGroup])) + numAnimalsInGroup*groupNum
#     portraits = np.asarray(flatten([port for port in portraits_df.loc[:,'portraits'] if len(port) == numAnimalsInGroup]))
#     bodies = np.asarray(flatten([port for port in portraits_df.loc[:,'bodies'] if len(port) == numAnimalsInGroup]))
#     bodyblobs = np.asarray(flatten([port for port in portraits_df.loc[:,'bodyblobs'] if len(port) == numAnimalsInGroup]))
#     centroids = np.asarray(flatten([centroids for centroids in portraits_df.loc[:,'centroids'] if len(centroids) == numAnimalsInGroup]))
#     noses = np.asarray(flatten([noses for noses in portraits_df.loc[:,'noses'] if len(noses) == numAnimalsInGroup]))
#     head_centroids = np.asarray(flatten([head_centroids for head_centroids in portraits_df.loc[:,'head_centroids'] if len(head_centroids) == numAnimalsInGroup]))
#     areas = np.asarray(flatten([areas for areas in portraits_df.loc[:,'areas'] if len(areas) == numAnimalsInGroup]))
#
#     if len(portraits) != len(labels):
#         raise ValueError('The number of portraits and labels should match.')
#     print('Group, ', groupNum)
#     print('Labels, ', labels)
#
#     return labels, portraits, bodies, bodyblobs, centroids, noses, head_centroids, areas
#
# def orderCenters(centers, video, transform):
#     """
#     orders points according to the video number and the transformation ('rotation', 'translation' or 'none')
#     this is supposed to work for the following libraries in the following way:
#     TU20160413 and TU20160428:
#         transform = 'rotation' (180 degrees)
#         video1:             video2:
#             0 3                 2 1
#             1 2                 3 0
#     TU20160920
#         video1:
#             1 0
#             2 3
#     TU20170131, TU20170201 and TU20170202:
#         video1:             video2:
#             1 0     7 6         7 6     1 0
#             2 3     4 5         4 5     2 3
#     Assuming that (0,0) is in the top-left corner because the centers come from the image
#     """
#     def shift(seq, n):
#         n = n % len(seq)
#         return seq[n:] + seq[:n]
#     def rearrange(cents):
#         newCents = []
#         newCents.append(cents[6])
#         newCents.append(cents[7])
#         newCents.append(cents[4])
#         newCents.append(cents[5])
#         newCents.append(cents[2])
#         newCents.append(cents[3])
#         newCents.append(cents[0])
#         newCents.append(cents[1])
#         return np.asarray(newCents)
#     #select the circle with minimal x (at most the arena can be 44deg rotated wrt its center)
#     cents = np.asarray(centers)
#     #centroid of the centers
#     centroid = np.true_divide(np.sum(cents, axis=0), cents.shape[0])
#     #put the centroid in the origin
#     trCenters = np.subtract(cents, centroid)
#     #compute the arctan to order
#     arctans = [np.arctan2(s[0],s[1]) for s in trCenters]
#     cents = cents[np.argsort(arctans)]
#     cents = list(tuple(map(tuple,cents)))
#     if video == 2 and transform == 'rotation':
#         cents = shift(cents, 2)
#     if video == 2 and transform == 'translation':
#         cents = rearrange(cents)
#     return cents
#
# def assignCenterFrame(centers,centroids,video, transform):
#     """
#     centers: centers of the arenas
#     centroids: centroids of the fish for a frame
#     """
#     centers = orderCenters(centers,video,transform) # Order the centers counterclockwise
#     d = scisd.cdist(centers,centroids) # Compute the distance of each fish to each centroid
#     identities = np.argmin(d,axis=0) # Assign identity by the centroid to which they are closer
#     return identities #, centers
#
# def assignCenterAndSave(path,centers, video, transform):
#     df, numSegment = loadFile(path, 'segmentation')
#     dfPermutations = pd.DataFrame(index=df.index,columns={'permutations'})
#     frameIndices = loadFile(path, 'frameIndices')
#     segmentIndices = frameIndices.loc[frameIndices.loc[:,'segment']==int(numSegment)]
#     segmentIndices = segmentIndices.index.tolist()
#     dfGlobal = pd.DataFrame(index = segmentIndices,columns={'identities','permutations','centroids','areas'})
#     for i, (centroids, index) in enumerate(zip(df.centroids,df.index)):
#         dfPermutations.loc[index,'permutations'] = assignCenterFrame(centers,centroids,video,transform)
#         dfGlobal.loc[segmentIndices[i],'identities'] = dfPermutations.loc[index,'permutations']
#         dfGlobal.loc[segmentIndices[i],'permutations'] = dfPermutations.loc[index,'permutations']
#         dfGlobal.loc[segmentIndices[i],'centroids'] = centroids
#         dfGlobal.loc[segmentIndices[i],'areas'] = df.loc[index,'areas']
#
#     df['permutations'] = dfPermutations['permutations']
#     saveFile(path, df, 'segment')
#     return dfGlobal
#
# def assignCenters(paths,centers,video = 1,transform = 'none'):
#     num_cores = multiprocessing.cpu_count()
#     # num_cores = 1
#
#     outDf = Parallel(n_jobs=num_cores)(delayed(assignCenterAndSave)(path, centers, video, transform) for path in paths)
#
#     dfGlobal = pd.concat(outDf)
#     dfGlobal = dfGlobal.sort_index(axis=0,ascending=True)
#     return dfGlobal
