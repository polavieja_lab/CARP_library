# Import standard libraries
from __future__ import division
import numpy as np
import multiprocessing
# Import third party libraries
import cv2
import pandas as pd
from joblib import Parallel, delayed
from sklearn.decomposition import PCA
# Import application/library specifics
from carp.generate.py_utils import loadFile, saveFile
from carp.generate.fishcontour import FishContour

def full2miniframe(point, boundingBox):
    """
    Push a point in the fullframe to miniframe coordinate system.
    Here it is use for centroids
    """
    return tuple(np.asarray(point) - np.asarray([boundingBox[0][0],boundingBox[0][1]]))

def get_portrait(miniframe, cnt, bb, identification_image_size, px_nose_above_center = 9):
    """Acquiring portraits from miniframe (for fish)

    Given a miniframe (i.e. a minimal rectangular image containing an animal)
    it returns a 36x36 image centered on the head.

    :param miniframe: A numpy 2-dimensional array
    :param cnt: A cv2-style contour, i.e. (x,:,y)
    :param bb: Coordinates of the left-top corner of miniframe in the big frame
    :param identification_image_size: size of the portrait (input image to cnn)
    :param px_nose_above_center: Number of pixels of nose above the center of portrait
    :return a smaller 2-dimensional array, and a tuple with all the nose coordinates in frame reference
    """
    # Extra parameters
    half_side_sq = int(identification_image_size/2)
    overhead = int(np.ceil(np.sqrt(half_side_sq**2 + (half_side_sq+px_nose_above_center)**2))) # Extra pixels when performing rotation, around sqrt(half_side_sq**2 + (half_side_sq+px_nose_above_center)**2)

    # Calculating nose coordinates in the full frame reference
    contour_cnt = FishContour.fromcv2contour(cnt)
    noseFull, rot_ang, head_centroid_full = contour_cnt.find_nose_and_orientation()

    # Calculating nose coordinates in miniframe reference
    nose = full2miniframe(noseFull,bb) #Float
    nose_pixels = np.array([int(nose[0]),int(nose[1])]) #int

    # Get roto-translation matrix and rotated miniframe
    # Rotation is performed around nose, nose coordinates stay constant
    # Final image gives an overhead above the nose coordinates, so the whole head should
    # stay visible in the final frame.
    # borderMode=cv2.BORDER_WRAP determines how source image is extended when needed
    M = cv2.getRotationMatrix2D(nose, rot_ang,1)
    minif_rot = cv2.warpAffine(miniframe, M, tuple(nose_pixels+overhead), borderMode=cv2.BORDER_WRAP, flags = cv2.INTER_CUBIC)

    # Crop the image in 32x32 frame around the nose
    x_range = xrange(nose_pixels[0]-half_side_sq,nose_pixels[0]+half_side_sq)
    y_range = xrange(nose_pixels[1]-half_side_sq+px_nose_above_center,nose_pixels[1]+half_side_sq+px_nose_above_center)
    portrait = minif_rot.take(y_range,mode='wrap',axis=0).take(x_range,mode='wrap',axis=1)

    return portrait, tuple(noseFull.astype('float32')), tuple(head_centroid_full.astype('float32')) #output as float because it is better for analysis.

def get_body(height, width, miniframe, pixels, bb, identificationImageSize, withBkg = False):
    """Acquiring portraits from miniframe (for flies)
    :param miniframe: A numpy 2-dimensional array
    :param cnt: A cv2-style contour, i.e. (x,:,y)
    :param bb: Coordinates of the left-top corner of miniframe in the big frame
    :param maximum_body_length: maximum body length of the blobs. It will be the size of the width and the height of the frame feed it to the CNN
    """
    if not withBkg:
        miniframe = only_blob_pixels(height, width, miniframe, pixels, bb)
    pca = PCA()
    pxs = np.unravel_index(pixels,(height,width))
    pxs1 = np.asarray(zip(pxs[0],pxs[1]))
    pca.fit(pxs1)
    rot_ang = 180 - np.arctan(pca.components_[0][1]/pca.components_[0][0])*180/np.pi - 45 # we substract 45 so that the fish is aligned in the diagonal. This say we have smaller frames
    center = (pca.mean_[1], pca.mean_[0])
    # print("PCA center before: ", center)
    center = full2miniframe(center, bb)
    center = np.array([int(center[0]), int(center[1])])
    # print("PCA center and angle: ", center, rot_ang)

    #rotate
    diag = np.sqrt(np.sum(np.asarray(miniframe.shape)**2)).astype(int)
    diag = (diag, diag)
    M = cv2.getRotationMatrix2D(tuple(center), rot_ang, 1)
    minif_rot = cv2.warpAffine(miniframe, M, diag, borderMode=cv2.BORDER_CONSTANT, flags = cv2.INTER_CUBIC)

    crop_distance = int(identificationImageSize/2)
    x_range = xrange(center[0] - crop_distance, center[0] + crop_distance)
    y_range = xrange(center[1] - crop_distance, center[1] + crop_distance)
    portrait = minif_rot.take(y_range, mode = 'wrap', axis=0).take(x_range, mode = 'wrap', axis=1)
    height, width = portrait.shape

    rot_ang_rad = rot_ang * np.pi / 180
    h_or_t_1 = np.array([np.cos(rot_ang_rad), np.sin(rot_ang_rad)]) * rot_ang_rad
    h_or_t_2 = - h_or_t_1
    # print(h_or_t_1,h_or_t_2,portrait.shape)
    return portrait, tuple(h_or_t_1.astype('int')), tuple(h_or_t_2.astype('int'))

def only_blob_pixels(height, width, miniframe, pixels, bb):
    pxs = np.array(np.unravel_index(pixels,(height, width))).T
    pxs = np.array([pxs[:, 0] - bb[0][1], pxs[:, 1] - bb[0][0]])
    temp_image = np.zeros_like(miniframe).astype('uint8')
    temp_image[pxs[0,:], pxs[1,:]] = 255
    temp_image = cv2.dilate(temp_image, np.ones((3,3)).astype('uint8'), iterations = 1)
    rows, columns = np.where(temp_image == 255)
    dilated_pixels = np.array([rows, columns])

    temp_image[dilated_pixels[0,:], dilated_pixels[1,:]] = miniframe[dilated_pixels[0,:], dilated_pixels[1,:]]
    return temp_image

def reaper(videoPath, frameIndices, height, width):
    # only function called from idTrackerDeepGUI
    print 'reaping', videoPath
    df, numSegment = loadFile(videoPath, 'segmentation')

    boundingboxes = np.asarray(df.loc[:, 'boundingBoxes']) #coordinate in the frame
    miniframes = np.asarray(df.loc[:, 'miniFrames']) #image containing the blob, same size
    miniframes = np.asarray(miniframes)
    contours = np.asarray(df.loc[:, 'contours'])
    centroidsSegment = np.asarray(df.loc[:,'centroids'])
    pixels = np.asarray(df.loc[:, 'pixels'])
    areasSegment = np.asarray(df.loc[:, 'areas'])

    segmentIndices = frameIndices.loc[frameIndices.loc[:,'segment']==int(numSegment)]
    segmentIndices = segmentIndices.index.tolist()

    """ Visualise """
    AllBodyBlobs = pd.DataFrame(index = segmentIndices, columns= ['bodyblobs'])
    AllminiFrames = pd.DataFrame(index = segmentIndices, columns= ['miniFrames'])
    AllCentroids= pd.DataFrame(index = segmentIndices, columns= ['centroids'])
    AllAreas = pd.DataFrame(index = segmentIndices, columns= ['areas'])

    counter = 0
    while counter < len(miniframes):
        bodyblobs = []
        areas = areasSegment[counter]
        bbs = boundingboxes[counter]
        minif = miniframes[counter]
        cnts = contours[counter]
        centroids = centroidsSegment[counter]
        pxs = pixels[counter]
        for j, miniframe in enumerate(minif):
            ### Uncomment to plot
            # cv2.imshow('frame', miniframe)
            # cv2.waitKey()
            identificationImageSize = 52
            bodyblob, _, _ = get_body(height, width, miniframe, pxs[j], bbs[j], identificationImageSize)
            # bodyblob, _, _ = get_body(height, width, miniframe, pxs[j], bbs[j], identificationImageSize, withBkg = True)
            # identificationImageSize = 36
            # portrait, _, _ = get_portrait(miniframe, cnts[j], bbs[j], identificationImageSize)
            bodyblobs.append(bodyblob)


        AllminiFrames.set_value(segmentIndices[counter], 'miniFrames', minif)
        AllBodyBlobs.set_value(segmentIndices[counter], 'bodyblobs', bodyblobs)
        AllCentroids.set_value(segmentIndices[counter], 'centroids', centroids)
        AllAreas.set_value(segmentIndices[counter], 'areas', areas)

        counter += 1
    print 'you just reaped', videoPath
    return AllminiFrames, AllBodyBlobs, AllCentroids, AllAreas

def portrait(videoPaths, dfGlobal, height, width):
    frameIndices = loadFile(videoPaths[0], 'frameIndices')
    num_cores = multiprocessing.cpu_count()
    # num_cores = 1
    out = Parallel(n_jobs=num_cores)(delayed(reaper)(videoPath, frameIndices, height, width) for videoPath in videoPaths)
    allminiFrames = [t[0] for t in out]
    allminiFrames = pd.concat(allminiFrames)
    allminiFrames = allminiFrames.sort_index(axis=0,ascending=True)

    allBodyBlobs = [t[1] for t in out]
    allBodyBlobs = pd.concat(allBodyBlobs)
    allBodyBlobs = allBodyBlobs.sort_index(axis=0,ascending=True)

    allCentroids = [t[2] for t in out]
    allCentroids = pd.concat(allCentroids)
    allCentroids = allCentroids.sort_index(axis=0, ascending=True)

    allAreas = [t[3] for t in out]
    allAreas = pd.concat(allAreas)
    allAreas = allAreas.sort_index(axis=0, ascending=True)

    if list(allBodyBlobs.index) != list(dfGlobal.index):
        raise ValueError('The list of indexes in allPortraits and dfGlobal should be the same')
    # dfGlobal1 = pd.DataFrame(index = range(len(dfGlobal)), columns=['images'])
    dfGlobal['identities'] = dfGlobal['permutations']
    dfGlobal['bodyblobs'] = allBodyBlobs
    dfGlobal['miniFrames'] = allminiFrames
    dfGlobal['centroids'] = allCentroids
    dfGlobal['areas'] = allAreas

    saveFile(videoPaths[0], dfGlobal['identities'], 'identities', hdfpkl='pkl')
    saveFile(videoPaths[0], dfGlobal['permutations'], 'permutations', hdfpkl='pkl')
    saveFile(videoPaths[0], dfGlobal['bodyblobs'], 'preprocessed_images', hdfpkl='pkl')
    saveFile(videoPaths[0], dfGlobal['miniFrames'], 'unprocessed_images', hdfpkl='pkl')
    saveFile(videoPaths[0], dfGlobal['centroids'], 'centroids', hdfpkl='pkl')
    saveFile(videoPaths[0], dfGlobal['areas'], 'areas', hdfpkl='pkl')
    # return dfGlobal
