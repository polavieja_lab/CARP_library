from __future__ import absolute_import, division, print_function
import numpy as np
import logging
# Import third party libraries
import cv2
from matplotlib.widgets import RectangleSelector
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import seaborn as sns
import pyautogui
import tkSimpleDialog
import tkFileDialog
import tkMessageBox
from Tkinter import Tk, Label, W, IntVar, Button, Checkbutton, Entry, mainloop
from carp.generate.segmentation import segmentVideo, blobExtractor
from carp.generate.py_utils import saveFile, loadFile
from carp.generate.get_portraits import get_body

sns.set(style="white", context="talk")


logger = logging.getLogger("__main__.GUI_utils")

def selectDir(initialDir, text = "Select folder"):
    root = Tk()
    root.withdraw()
    Label(root, text=text).grid(row=0, sticky=W)
    dirName = tkFileDialog.askdirectory(initialdir = initialDir)
    root.destroy()
    return dirName

def getInput(name,text):
    root = Tk() # dialog needs a root window, or will create an "ugly" one for you
    root.withdraw() # hide the root window
    inputString = tkSimpleDialog.askstring(name, text, parent=root)
    root.destroy() # clean up after yourself!
    return inputString.lower()

def selectOptions(optionsList, optionsDict=None, text="Select preprocessing options:  "):
    master = Tk()
    if optionsDict==None:
        optionsDict = {el:'1' for el in optionsList}
    def createCheckBox(name,i):
        var = IntVar()
        Checkbutton(master, text=name, variable=var).grid(row=i+1, sticky=W)
        return var

    Label(master, text=text).grid(row=0, sticky=W)
    variables = []
    for i, opt in enumerate(optionsList):
        if optionsDict[opt] == '1':
            var = createCheckBox(opt,i)
            variables.append(var)
            var.set(optionsDict[opt])
        else:
            Label(master, text= '     ' + opt).grid(row=i+1, sticky=W)
            var = IntVar()
            var.set(0)
            variables.append(var)

    Button(master, text='Ok', command=master.quit).grid(row=i+2, sticky=W, pady=4)
    mainloop()
    varValues = []
    for var in variables:
        varValues.append(var.get())
    optionsDict = dict((key, value) for (key, value) in zip(optionsList, varValues))
    master.destroy()
    return optionsDict











"""
Display messages and errors
"""
def load_previous_dict_check(processes, loadPreviousDict):
    zero_key_indices = [ind for ind, key in enumerate(processes)
                        if loadPreviousDict[key] == 0]
    if len(zero_key_indices) > 0:
        for i, p in enumerate(processes):
            if i > zero_key_indices[0]: loadPreviousDict[p] = 0

    for key in loadPreviousDict:
        if loadPreviousDict[key] == -1: loadPreviousDict[key] = 0
    return loadPreviousDict

def selectFile():
    root = Tk()
    root.withdraw()
    filename = tkFileDialog.askopenfilename()
    root.destroy()
    return filename

def displayMessage(title,message):
    window = Tk()
    window.wm_withdraw()
    window.geometry("1x1+"+str(window.winfo_screenwidth()/2)+"+"+str(window.winfo_screenheight()/2))
    tkMessageBox.showinfo(title=title, message=message)

def displayError(title, message):
    window = Tk()
    window.wm_withdraw()
    window.geometry("1x1+200+200")
    tkMessageBox.showerror(title=title,message=message,parent=window)

def getMultipleInputs(winTitle, inputTexts):
    #Gui Things
    def retrieve_inputs():
        global inputs
        inputs = [var.get() for var in variables]
        window.destroy()
        return inputs
    window = Tk()
    window.title(winTitle)
    variables = []

    for inputText in inputTexts:
        text = Label(window, text =inputText)
        guess = Entry(window)
        variables.append(guess)
        text.pack()
        guess.pack()
    finished = Button(text="ok", command=retrieve_inputs)
    finished.pack()
    window.mainloop()
    return inputs

'''****************************************************************************
Resolution reduction
****************************************************************************'''
def check_resolution_reduction(video, old_video, usePreviousRR):
    if video.reduce_resolution:
        if usePreviousRR and old_video.resolution_reduction is not None:
            if hasattr(old_video, 'resolution_reduction'):
                return old_video.resolution_reduction
            else:
                return float(getInput('Resolution reduction', 'Resolution reduction parameter not found in previous video. \nInput the resolution reduction factor (.5 would reduce by half): '))
        else:
            return float(getInput('Resolution reduction', 'Input the resolution reduction factor (.5 would reduce by half): '))
    else:
        return 1


''' ****************************************************************************
ROI selector GUI
*****************************************************************************'''
def getMask(im):
    """Returns a uint8 mask following openCV convention
    as used in segmentation (0 invalid, 255 valid)
    adapted from: matplotlib.org/examples/widgets/rectangle_selector.py
    """
    def line_select_callback(eclick, erelease):
        'eclick and erelease are the press and release events'
        global coord
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        coord = (x1, y1, x2, y2)

    def toggle_selector(event):
        global coord
        if event.key == 'r':
            coordinates.append(coord)
            c = coordinates[-1]
            p = patches.Rectangle((c[0], c[1]), c[2]-c[0], c[3]-c[1],alpha=0.4)
            p1 = patches.Rectangle((c[0], c[1]), c[2]-c[0], c[3]-c[1],facecolor="white")
            current_ax.add_patch(p)
            plt.draw()
            coord = np.asarray(c).astype('int')
            cv2.rectangle(maskout,(coord[0],coord[1]),(coord[2],coord[3]),255,-1)
            centers.append(None)
        if event.key == 'c':
            coordinates.append(coord)
            c = coordinates[-1]
            w = c[2]-c[0]
            h = c[3]-c[1]
            p = patches.Ellipse((c[0]+w/2, c[1]+h/2), w, h, angle=0.0, alpha=0.4)
            p1 = patches.Ellipse((c[0]+w/2, c[1]+h/2), w, h, angle=0.0,facecolor="white")
            current_ax.add_patch(p)
            plt.draw()
            coord = np.asarray(c).astype('int')
            center = ((coord[2] + coord[0]) // 2,(coord[3] + coord[1]) // 2)
            angle = 90
            axes = tuple(sorted(((coord[2] - coord[0]) // 2,(coord[3] - coord[1]) //2)))
            cv2.ellipse(maskout,center,axes,angle,0,360,255,-1)
            centers.append(center)

    coordinates = []
    centers = []
    #visualise frame in full screen
    w, h = pyautogui.size()
    fig, ax_arr = plt.subplots(1,1, figsize=(w/96,h/96))
    fig.suptitle('Select mask')
    current_ax = ax_arr
    current_ax.set_title('Drag on the image, adjust,\n press r, or c to get a rectangular or a circular ROI')
    sns.despine(fig=fig, top=True, right=True, left=True, bottom=True)
    current_ax.imshow(im, cmap = 'gray')
    mask = np.zeros_like(im)
    maskout = np.zeros_like(im,dtype='uint8')
    toggle_selector.RS = RectangleSelector(current_ax, line_select_callback,
                                           drawtype='box', useblit=True,
                                           button=[1, 3],  # don't use middle button
                                           minspanx=5, minspany=5,
                                           spancoords='pixels',
                                           interactive=True)
    plt.connect('key_press_event', toggle_selector)
    plt.show()

    return maskout, centers

def checkROI_library(useROI, usePreviousROI, frame, videoPath):
    ''' Select ROI '''
    if useROI:
        if usePreviousROI:
            mask = loadFile(videoPath, 'ROI')
            mask = np.asarray(mask)
            centers= loadFile(videoPath, 'centers')
            centers = np.asarray(centers) ### TODO maybe we need to pass to a list of tuples
        else:
            logger.debug('Selecting ROI...')
            mask, centers = getMask(frame)
            # mask = adaptROI(mask)
    else:
        logger.debug('No ROI selected...')
        mask = np.ones_like(frame)*255
        centers = []
    return mask, centers

def ROISelectorPreview_library(videoPaths, useROI, usePreviousROI, numSegment=0):
    """
    loads a preview of the video for manual fine-tuning
    """
    cap2 = cv2.VideoCapture(videoPaths[0])
    flag, frame = cap2.read()
    cap2.release()
    height = frame.shape[0]
    width = frame.shape[1]
    frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    mask, centers = checkROI_library(useROI, usePreviousROI, frameGray, videoPaths[0])
    saveFile(videoPaths[0], mask, 'ROI')
    saveFile(videoPaths[0], centers, 'centers')
    return width, height, mask, centers

''' ****************************************************************************
First preview numAnimals, inspect parameters for segmentation and portraying
**************************************************************************** '''
def SegmentationPreview_library(videoPaths, width, height, bkg, mask, useBkg, preprocParams, frameIndices, size = 1):

    ### FIXME Currently the scale factor of the image is not passed everytime we change the segment. It need to be changed so that we do not need to resize everytime we open a new segmen.
    minArea = preprocParams['minArea']
    maxArea = preprocParams['maxArea']
    minThreshold = preprocParams['minThreshold']
    maxThreshold = preprocParams['maxThreshold']
    numAnimals = preprocParams['numAnimals']

    if numAnimals == None:
        numAnimals = getInput('Number of animals','Type the number of animals')
        numAnimals = int(numAnimals)

    global cap, currentSegment
    currentSegment = 0
    cap = cv2.VideoCapture(videoPaths[0])
    numFrames = len(frameIndices)

    def thresholder(minTh, maxTh):
        toile = np.zeros_like(frameGray, dtype='uint8')
        segmentedFrame = segmentVideo(avFrame, minTh, maxTh, bkg, mask, useBkg)
        #contours, hierarchy = cv2.findContours(segmentedFrame,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        maxArea = cv2.getTrackbarPos('maxArea', 'Bars')
        minArea = cv2.getTrackbarPos('minArea', 'Bars')
        bbs, miniFrames, _, areas, pixels, goodContours, estimated_body_lengths = blobExtractor(segmentedFrame, frameGray, minArea, maxArea)
        cv2.drawContours(toile, goodContours, -1, color=255, thickness = -1)
        shower = cv2.addWeighted(frameGray,1,toile,.5,0)
        showerCopy = shower.copy()
        resUp = cv2.getTrackbarPos('ResUp', 'Bars')
        resDown = cv2.getTrackbarPos('ResDown', 'Bars')
        showerCopy = cv2.resize(showerCopy,None,fx = resUp, fy = resUp)
        showerCopy = cv2.resize(showerCopy,None, fx = np.true_divide(1,resDown), fy = np.true_divide(1,resDown))
        numColumns = 5
        numGoodContours = len(goodContours)
        numBlackImages = numColumns - numGoodContours % numColumns
        numImages = numGoodContours + numBlackImages
        j = 0
        maximum_body_length = 70
        if estimated_body_lengths:
            maximum_body_length = np.max(estimated_body_lengths)
        imagesMat = []
        rowImage = []

        print("num blobs detected: %i" %numGoodContours)
        print("maximum_body_length: %i" %maximum_body_length)
        print("areas: %s" %str(areas))
        identificationImageSize = int(np.sqrt(maximum_body_length ** 2 / 2))
        identificationImageSize = identificationImageSize + identificationImageSize%2  #this is to make the identificationImageSize even

        while j < numImages:
            if j < numGoodContours:
                image_for_identification, _, _ = get_body(height, width, miniFrames[j], pixels[j], bbs[j], identificationImageSize)
            else:
                image_for_identification = np.zeros((identificationImageSize,identificationImageSize),dtype='uint8')
            print(image_for_identification.shape)
            rowImage.append(image_for_identification)
            if (j+1) % numColumns == 0:
                imagesMat.append(np.hstack(rowImage))
                rowImage = []
            j += 1

        imagesMat = np.vstack(imagesMat)
        cv2.imshow('Bars',np.squeeze(imagesMat))
        cv2.imshow('IdPlayer', showerCopy)
        cv2.moveWindow('Bars', 10,10 )
        cv2.moveWindow('IdPlayer', 200, 10 )

    def scroll(trackbarValue):
        global frame, avFrame, frameGray, cap, currentSegment

        # Select segment dataframe and change cap if needed
        sNumber = frameIndices.loc[trackbarValue,'segment']
        sFrame = frameIndices.loc[trackbarValue,'frame']

        if sNumber != currentSegment: # we are changing segment
            logger.debug('Changing segment...')
            currentSegment = sNumber

            if len(videoPaths) > 1:
                cap = cv2.VideoCapture(videoPaths[sNumber-1])

        #Get frame from video file
        if len(videoPaths) > 1:
            cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,sFrame)
        else:
            cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,trackbarValue)
        ret, frame = cap.read()

        frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        avIntensity = np.float32(np.mean(frameGray))
        avFrame = np.divide(frameGray,avIntensity)
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass


    def changeMinTh(minTh):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    def changeMaxTh(maxTh):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    def changeMinArea(x):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    def changeMaxArea(maxArea):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    def resizeImageUp(res):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    def resizeImageDown(res):
        minTh = cv2.getTrackbarPos('minTh', 'Bars')
        maxTh = cv2.getTrackbarPos('maxTh', 'Bars')
        thresholder(minTh, maxTh)
        pass

    cv2.createTrackbar('start', 'Bars', 0, numFrames-1, scroll )
    cv2.createTrackbar('minTh', 'Bars', 0, 255, changeMinTh)
    cv2.createTrackbar('maxTh', 'Bars', 0, 255, changeMaxTh)
    cv2.createTrackbar('minArea', 'Bars', 0, 1000, changeMinArea)
    cv2.createTrackbar('maxArea', 'Bars', 0, 60000, changeMaxArea)
    cv2.createTrackbar('ResUp', 'Bars', 1, 20, resizeImageUp)
    cv2.createTrackbar('ResDown', 'Bars', 1, 20, resizeImageDown)

    defFrame = 1
    defMinTh = minThreshold
    defMaxTh = maxThreshold
    defMinA = minArea
    defMaxA = maxArea
    defRes = size

    scroll(defFrame)
    cv2.setTrackbarPos('start', 'Bars', defFrame)
    changeMaxArea(defMaxA)
    cv2.setTrackbarPos('maxArea', 'Bars', defMaxA)
    changeMinArea(defMinA)
    cv2.setTrackbarPos('minArea', 'Bars', defMinA)
    changeMinTh(defMinTh)
    cv2.setTrackbarPos('minTh', 'Bars', defMinTh)
    changeMaxTh(defMaxTh)
    cv2.setTrackbarPos('maxTh', 'Bars', defMaxTh)
    resizeImageUp(defRes)
    cv2.setTrackbarPos('ResUp', 'Bars', defRes)
    resizeImageDown(defRes)
    cv2.setTrackbarPos('ResDown', 'Bars', defRes)

    cv2.waitKey(0)

    preprocParams = {
                'minThreshold': cv2.getTrackbarPos('minTh', 'Bars'),
                'maxThreshold': cv2.getTrackbarPos('maxTh', 'Bars'),
                'minArea': cv2.getTrackbarPos('minArea', 'Bars'),
                'maxArea': cv2.getTrackbarPos('maxArea', 'Bars'),
                'numAnimals': numAnimals}

    cap.release()
    cv2.destroyAllWindows()

    cv2.waitKey(1)
    cv2.destroyAllWindows()
    cv2.waitKey(1)

    return preprocParams

def selectPreprocParams_library(videoPaths, usePreviousPrecParams, width, height, bkg, mask, useBkg, frameIndices):
    if not usePreviousPrecParams:
        preprocParams = {
                    'minThreshold': 0,
                    'maxThreshold': 155,
                    'minArea': 150,
                    'maxArea': 60000,
                    'numAnimals': None}
        preprocParams = SegmentationPreview_library(videoPaths, width, height, bkg, mask, useBkg, preprocParams, frameIndices)

        cv2.waitKey(1)
        cv2.destroyAllWindows()
        cv2.waitKey(1)

        saveFile(videoPaths[0], preprocParams, 'preprocparams',hdfpkl='pkl')
    else:
        preprocParams = loadFile(videoPaths[0], 'preprocparams',hdfpkl='pkl')
    return preprocParams

''' ****************************************************************************
Fragmentation inspector
*****************************************************************************'''
def playFragmentation_library(videoPaths,segmPaths,dfGlobal,visualize = False):
    """
    IdInspector
    """
    if visualize:
        info = loadFile(videoPaths[0], 'videoInfo', hdfpkl = 'pkl')
        width = info['width']
        height = info['height']
        numAnimals = info['numAnimals']
        maxNumBlobs = info['maxNumBlobs']
        numSegment = 0
        frameIndices = loadFile(videoPaths[0], 'frameIndices')
        path = videoPaths[numSegment]

        def IdPlayerFragmentation(videoPaths,segmPaths,numAnimals, width, height,frameIndices):

            global segmDf, cap, currentSegment
            segmDf,sNumber = loadFile(segmPaths[0], 'segmentation')
            currentSegment = int(sNumber)
            logger.debug('Visualizing video %s' % path)
            cap = cv2.VideoCapture(videoPaths[0])
            numFrames = len(frameIndices)
            # numFrame = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))

            def onChange(trackbarValue):
                global segmDf, cap, currentSegment
                # Select segment dataframe and change cap if needed
                sNumber = frameIndices.loc[trackbarValue,'segment']
                sFrame = frameIndices.loc[trackbarValue,'frame']
                if sNumber != currentSegment: # we are changing segment
                    logger.debug('Changing segment...')
                    prevSegmDf, _ = loadFile(segmPaths[sNumber-2], 'segmentation')
                    segmDf, _ = loadFile(segmPaths[sNumber-1], 'segmentation')
                    currentSegment = sNumber

                    if len(videoPaths) > 1:
                        cap = cv2.VideoCapture(videoPaths[sNumber-1])
                #Get frame from video file
                if len(videoPaths) > 1:
                    cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,sFrame)
                else:
                    cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,trackbarValue)
                ret, frame = cap.read()
                font = cv2.FONT_HERSHEY_SIMPLEX
                #Color to gray scale
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                font = cv2.FONT_HERSHEY_SIMPLEX

                print(segmDf.columns)
                pixelsB = segmDf.loc[sFrame,'pixels']
                centroids = segmDf.loc[sFrame,'centroids']
                permutation = segmDf.loc[sFrame,'permutations']
                # Plot segmentated blobs
                for i, pixel in enumerate(pixelsB):
                    px = np.unravel_index(pixel,(height,width))
                    frame[px[0],px[1]] = 255

                for i, centroid in enumerate(centroids):
                    cv2.putText(frame, 'i' + str(permutation[i]) + '|h' + str(i), tuple(map(int, centroid)), font, .7, 0)

                cv2.putText(frame,str(trackbarValue),(50,50), font, 3,(255,0,0))

                # Visualization of the process
                cv2.imshow('IdPlayerFragmentation',frame)
                pass

            cv2.namedWindow('IdPlayerFragmentation')
            cv2.createTrackbar( 'start', 'IdPlayerFragmentation', 0, numFrames-1, onChange )

            onChange(1)
            cv2.waitKey()

        IdPlayerFragmentation(videoPaths,segmPaths,numAnimals, width, height,frameIndices)
