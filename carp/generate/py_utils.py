from __future__ import division
import os
import glob
import re
import datetime
import pandas as pd
import cPickle as pickle
import logging

# sys.path.append('../utils')

logger = logging.getLogger("__main__.py_utils")


def scanFolder(path):
    ### NOTE if the video selected does not finish with '_1' the scanFolder function won't select all of them. This can be improved
    paths = [path]
    video = os.path.basename(path)
    filename, extension = os.path.splitext(video)
    folder = os.path.dirname(path)
    # maybe write check on video extension supported by opencv2
    if filename[-2:] == '_1':
        paths = natural_sort(glob.glob(folder + "/" + filename[:-1] + "*" + extension))
    return paths


def getExistentFiles_library(path, listNames, segmPaths):
    """
    get processes already computed in a previous session
    """
    existentFile = {name:'0' for name in listNames}
    video = os.path.basename(path)
    folder = os.path.dirname(path)

    # createFolder(path)

    #count how many videos we have
    numSegments = len(segmPaths)

    filename, extension = os.path.splitext(video)
    subFolders = glob.glob(folder +"/*/")

    srcSubFolder = folder + '/preprocessing/'
    for name in listNames:
        if name == 'segmentation':
            segDirname = srcSubFolder + name
            if os.path.isdir(segDirname):
                print 'Segmentation folder exists'
                numSegmentedVideos = len(glob.glob1(segDirname,"*.pkl"))
                print
                if numSegmentedVideos == numSegments:
                    print 'The number of segments and videos is the same'
                    existentFile[name] = '1'
        else:
            extensions = ['.pkl', '.hdf5']
            for ext in extensions:
                fullFileName = srcSubFolder + '/' + name + ext
                if os.path.isfile(fullFileName):
                    existentFile[name] = '1'

    return existentFile, srcSubFolder


def loadFile(path, name, hdfpkl = 'hdf',sessionPath = ''):
    """
    loads a pickle. path is the path of the video, while name is a string in the
    set {}
    """
    video = os.path.basename(path)
    folder = os.path.dirname(path)
    filename, extension = os.path.splitext(video)
    subfolder = ''

    if name  == 'segmentation':
        subfolder = '/preprocessing/segmentation/'
        nSegment = filename.split('_')[-1]
        if hdfpkl == 'hdf':
            filename = 'segm_' + nSegment + '.pkl'
            return pd.read_pickle(folder + subfolder + filename ), nSegment
        elif hdfpkl == 'pkl':
            filename = 'segm_' + nSegment + '.pkl'
            return pickle.load(open(folder + subfolder + filename) ,'rb'), nSegmen
    elif name == 'statistics':
        filename = 'statistics.pkl'
        return pickle.load(open(sessionPath + '/' + filename,'rb') )
    elif name == 'trajectories':
        filename = 'trajectories.pkl'
        return pickle.load(open(sessionPath + '/' + filename,'rb') )
    else:
        subfolder = '/preprocessing/'
        if hdfpkl == 'hdf':
            filename = name + '.pkl'
            return pd.read_pickle(folder + subfolder + filename )
        elif hdfpkl == 'pkl':
            filename = name + '.pkl'
            return pickle.load(open(folder + subfolder + filename,'rb') )

    print 'You just loaded ', folder + subfolder + filename


def saveFile(path, variabletoSave, name, hdfpkl = 'hdf',sessionPath = '', nSegment = None):
    import cPickle as pickle
    """
    All the input are strings!!!
    path: path to the first segment of the video
    name: string to add to the name of the video (wihtout timestamps)
    folder: path to the folder in which the file has to be stored
    """
    # if os.path.exists(path)==False:
    #     raise ValueError("the video %s does not exist!" %path)
    video = os.path.basename(path)
    filename, extension = os.path.splitext(video)
    folder = os.path.dirname(path)
    filename, extension = os.path.splitext(video)

    if name == 'segment' or name == 'segmentation':
        subfolder = '/preprocessing/segmentation/'
        if nSegment == None:
            nSegment = filename.split('_')[-1]# and before the number of the segment
        if hdfpkl == 'hdf':
            filename = 'segm_' + nSegment + '.pkl'
            pathToSave = folder + subfolder + filename
            variabletoSave.to_pickle(pathToSave)
        elif hdfpkl == 'pkl':
            filename = 'segm_' + nSegment + '.pkl'
            pathToSave = folder + subfolder+ filename
            pickle.dump(variabletoSave,open(pathToSave,'wb'))
    elif name == 'trajectories':
        filename = 'trajectories.pkl'
        pathToSave = sessionPath + '/' + filename
        pickle.dump(variabletoSave,open(pathToSave,'wb'))
    else:
        subfolder = '/preprocessing/'
        if hdfpkl == 'hdf':
            filename = name + '.pkl'
            if isinstance(variabletoSave, dict):
                variabletoSave = pd.DataFrame.from_dict(variabletoSave,orient='index')
            elif not isinstance(variabletoSave, pd.DataFrame):
                variabletoSave = pd.DataFrame(variabletoSave)
            pathToSave = folder + subfolder + filename
            variabletoSave.to_pickle(pathToSave)
        elif hdfpkl == 'pkl':
            filename = name + '.pkl'
            # filename = os.path.relpath(filename)
            pathToSave = folder + subfolder + filename
            pickle.dump(variabletoSave,open(pathToSave,'wb'))

    print 'You just saved ', pathToSave


def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)


def createFolder(path, name = '', timestamp = False):

    folder = os.path.dirname(path)
    folderName = folder +'/preprocessing'
    if timestamp :
        ts = '{:%Y%m%d%H%M%S}_'.format(datetime.datetime.now())
        folderName = folderName + '_' + ts

    if os.path.isdir(folderName):
        print 'Preprocessing folder exists'
        subFolder = folderName + '/segmentation'
        if os.path.isdir(subFolder):
            print 'Segmentation folder exists'
        else:
            os.makedirs(subFolder)
            print subFolder + ' has been created'
    else:
        os.makedirs(folderName)
        print folderName + ' has been created'
        subFolder = folderName + '/segmentation'
        os.makedirs(subFolder)
        print subFolder + ' has been created'


def flatten(l):
    ''' flatten a list of lists '''
    try:
        ans = [inner for outer in l for inner in outer]
    except:
        ans = [y for x in l for y in (x if isinstance(x, tuple) else (x,))]
    return ans
