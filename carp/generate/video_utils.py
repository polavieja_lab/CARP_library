from __future__ import absolute_import, division, print_function
# Import standard libraries
import os
import numpy as np
import multiprocessing
# Import third party libraries
import cv2
from joblib import Parallel, delayed
import pandas as pd
# Import application/library specifics
from carp.generate.py_utils import saveFile, flatten, loadFile

"""
Get general information from video
"""
def getVideoInfo(videoPaths):
    if len(videoPaths) == 1:
        videoPath = videoPaths
    elif len(videoPaths) > 1:
        videoPath = videoPaths[0]
    else:
        raise ValueError('the videoPath (or list of videoPaths) seems to be empty')
    cap = cv2.VideoCapture(videoPaths[0])
    width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    return width, height

def getNumFrame(videoPath):
    cap = cv2.VideoCapture(videoPath)
    return int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))

def collectAndSaveVideoInfo(videoPath, numFrames, height, width, numAnimals, numCores, minThreshold,maxThreshold,maxArea,maxNumBlobs):
    """
    saves general info about the video in a pickle (_videoinfo.pkl)
    """
    videoInfo = {
        'path': videoPath,
        'numFrames': numFrames,
        'height':height,
        'width': width,
        'numAnimals':numAnimals,
        'numCores':numCores,
        'minThreshold':minThreshold,
        'maxThreshold':maxThreshold,
        'maxArea': maxArea,
        'maxNumBlobs':maxNumBlobs
        }
    print('videoInfo, ', videoInfo)
    saveFile(videoPath, videoInfo, 'videoInfo',hdfpkl='pkl')

def generateVideoTOC(allSegments, videoPath):
    """
    generates a dataframe mapping frames to segments and save it as pickle
    """
    segmentsTOC = []
    framesTOC = []
    for segment, frame in allSegments:
        segmentsTOC.append(segment)
        framesTOC.append(frame)
    segmentsTOC = flatten(segmentsTOC)
    framesTOC = flatten(framesTOC)
    videoTOC =  pd.DataFrame({'segment':segmentsTOC, 'frame': framesTOC})
    numFrames = len(videoTOC)
    saveFile(videoPath, videoTOC, 'frameIndices')
    return numFrames

def getSegmPaths(videoPaths,framesPerSegment=500):
    folder = os.path.dirname(videoPaths[0])
    print(folder)
    if len(videoPaths) == 1: # The video is in a single filename
        numFrames = getNumFrame(videoPaths[0])
        numSegments = np.ceil(np.true_divide(numFrames,framesPerSegment)).astype('int')
        framesTOC = [frame % framesPerSegment for frame in range(numFrames)]
        segmentsTOC = [frame/framesPerSegment+1 for frame in range(numFrames)]
        segmPaths = [folder + '/segm_%s.pkl' %str(seg+1) for seg in range(numSegments)]
    else:
        framesTOC = [range(0,getNumFrame(videoPath)) for videoPath in videoPaths]
        segmentsTOC = [[seg+1 for i in range(getNumFrame(videoPath))] for seg, videoPath in enumerate(videoPaths)]
        framesTOC = flatten(framesTOC)
        segmentsTOC = flatten(segmentsTOC)
        segmPaths = videoPaths

    frameIndices =  pd.DataFrame({'segment':segmentsTOC, 'frame': framesTOC})
    saveFile(videoPaths[0], frameIndices, 'frameIndices')
    return frameIndices, segmPaths

"""
Compute background and threshold
"""
def computeBkgParSingleVideo(startingFrame,endingFrame,videoPath,bkg,framesPerSegment):
    # Open cap
    cap = cv2.VideoCapture(videoPath)
    print('Adding from starting frame %i to background' %startingFrame)
    numFramesBkg = 0
    frameInds = range(startingFrame,endingFrame,100)
    for ind in frameInds:
        cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,ind)
        ret, frameBkg = cap.read()
        gray = cv2.cvtColor(frameBkg, cv2.COLOR_BGR2GRAY)
        # gray = checkEq(EQ, gray)
        gray = np.true_divide(gray,np.mean(gray))
        bkg = bkg + gray
        numFramesBkg += 1
    cap.release()

    return bkg, numFramesBkg

def computeBkgParSegmVideo(videoPath,bkg):
    print('Adding video %s to background' % videoPath)
    cap = cv2.VideoCapture(videoPath)
    counter = 0
    numFrame = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    numFramesBkg = 0
    frameInds = range(0,numFrame,100)
    for ind in frameInds:
        cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,ind)
        ret, frameBkg = cap.read()
        gray = cv2.cvtColor(frameBkg, cv2.COLOR_BGR2GRAY)
        gray = np.true_divide(gray,np.mean(gray))
        bkg = bkg + gray
        numFramesBkg += 1

    return bkg, numFramesBkg

def computeBkg(videoPaths, width, height):
    # This holds even if we have not selected a ROI because then the ROI is
    # initialized as the full frame
    bkg = np.zeros((height,width))
    num_cores = multiprocessing.cpu_count()
    # num_cores = 1
    if len(videoPaths) == 1: # one single video
        print('one single video, computing bkg in parallel from single video')
        videoPath = videoPaths[0]
        frameIndices = loadFile(videoPaths[0], 'frameIndices')
        framesPerSegment = len(np.where(frameIndices.loc[:,'segment'] == 1)[0])
        segments = np.unique(frameIndices.loc[:,'segment'])
        startingFrames = [frameIndices[frameIndices['segment']==seg].index[0] for seg in segments]
        endingFrames = [frameIndices[frameIndices['segment']==seg].index[-1] for seg in segments]
        output = Parallel(n_jobs=num_cores)(delayed(computeBkgParSingleVideo)(startingFrame,endingFrames,videoPath,bkg,framesPerSegment) for startingFrame, endingFrames in zip(startingFrames,endingFrames))
    else: # multiple segments video
        output = Parallel(n_jobs=num_cores)(delayed(computeBkgParSegmVideo)(videoPath,bkg) for videoPath in videoPaths)

    partialBkg = [bkg for (bkg,_) in output]
    totNumFrame = np.sum([numFrame for (_,numFrame) in output])
    bkg = np.sum(np.asarray(partialBkg),axis=0)
    bkg = np.true_divide(bkg, totNumFrame)
    return bkg

def checkBkg(videoPaths, useBkg, usePreviousBkg, EQ, width, height):
    videoPath = videoPaths[0]
    if useBkg:
        if usePreviousBkg:
            bkg = loadFile(videoPath, 'bkg',hdfpkl='pkl')
        else:
            bkg = computeBkg(videoPaths, width, height)
            saveFile(videoPath, bkg, 'bkg', hdfpkl='pkl')
        return bkg.astype(np.float32)
    else:
        return None

def getSegmPaths(videoPaths,framesPerSegment=500):
    folder = os.path.dirname(videoPaths[0])
    print(folder)
    if len(videoPaths) == 1: # The video is in a single filename
        numFrames = getNumFrame(videoPaths[0])
        numSegments = np.ceil(np.true_divide(numFrames,framesPerSegment)).astype('int')
        framesTOC = [frame % framesPerSegment for frame in range(numFrames)]
        segmentsTOC = [int(frame/framesPerSegment)+1 for frame in range(numFrames)]
        segmPaths = [folder + '/segm_%s.pkl' %str(seg+1) for seg in range(numSegments)]
    else:
        framesTOC = [range(0,getNumFrame(videoPath)) for videoPath in videoPaths]
        segmentsTOC = [[seg+1 for i in range(getNumFrame(videoPath))] for seg, videoPath in enumerate(videoPaths)]
        framesTOC = flatten(framesTOC)
        segmentsTOC = flatten(segmentsTOC)
        segmPaths = videoPaths

    frameIndices =  pd.DataFrame({'segment':segmentsTOC, 'frame': framesTOC})
    saveFile(videoPaths[0], frameIndices, 'frameIndices')
    return frameIndices, segmPaths

"""
Get information from blobs
"""
def filterContoursBySize(contours,minArea,maxArea):
    goodContours = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > minArea and area < maxArea:
            goodContours.append(contour)
    return goodContours

def coordROI2Full(x,y,ROI):
    if ROI[0][0] > ROI[1][0]:
        ROI = [ROI[1],ROI[0]]
    x = x + ROI[0][0]
    y = y + ROI[0][1]
    return x, y

def cntROI2OriginalFrame(contours,ROI):
    contoursFull = []
    for cnt in contours:
        cnt = cnt + np.asarray([ROI[0][0],ROI[0][1]])
        contoursFull.append(cnt)
    return contoursFull

def cnt2BoundingBox(cnt,boundingBox):
    return cnt - np.asarray([boundingBox[0][0],boundingBox[0][1]])

def full2BoundingBox(point, boundingBox):
    return point - np.asarray([boundingBox[0][0],boundingBox[0][1]])

def cntBB2Full(cnt,boundingBox):
    return cnt + np.asarray([boundingBox[0][0],boundingBox[0][1]])

def getBoundigBox(cnt, width, height, crossing_detector = False):
    x,y,w,h = cv2.boundingRect(cnt)
    original_diagonal = int(np.ceil(np.sqrt(w**2 + h**2)))
    nl = int(np.ceil((100 - w) / 2))
    nr = int(np.floor((100 - w) / 2))
    nt = int(np.ceil((100 - h) / 2))
    nb = int(np.floor((100 - h) / 2))
    if x - nl > 0:
        x = x - nl
    else:
        x = 0
    if y - nt > 0:
        y = y - nt
    else:
        y = 0
    if x + w + nl + nr < width:
        w = w + nr + nl
    else:
        w = width - x
    if y + h + nt + nb < height:
        h = h + nt + nb
    else:
        h = height - y
    return ((x, y),(x + w, y + h)), original_diagonal

def boundingBox_ROI2Full(bb, ROI):
    """
    Gives back only the first point of bb translated in the full frame
    """
    return ((bb[0][0] + ROI[0][0], bb[0][1] + ROI[0][1]),(bb[1][0] + ROI[0][0], bb[1][1] + ROI[0][1]))

def getCentroid(cnt):
    M = cv2.moments(cnt)

    x = M['m10']/M['m00']
    y = M['m01']/M['m00']
    # print(x,y)

    # x, y = coordROI2Full(x,y,ROI)
    return (x,y)

def getPixelsList(cnt, width, height):
    cimg = np.zeros((height, width))
    cv2.drawContours(cimg, [cnt], -1, color=255, thickness = -1)
    # Access the image pixels and create a 1D numpy array then add to list
    pts = np.where(cimg == 255)
    return zip(pts[0],pts[1])

def sampleBkg(cntBB, miniFrame):
    #FIXME TO BE REMOVED
    frame = np.zeros((500,500)).astype('uint8')
    cv2.drawContours(miniFrame, [cntBB], -1, color=255, thickness = -1)
    # Access the image pixels and create a 1D numpy array then add to list
    bkgSample = miniFrame[np.where(miniFrame != 255)]
    return bkgSample

def getMiniFrame(frame, cnt):
    height = frame.shape[0]
    width = frame.shape[1]
    boundingBox, estimated_body_length = getBoundigBox(cnt, width, height) # the estimated body length is the diagonal of the original boundingBox
    miniFrame = frame[boundingBox[0][1]:boundingBox[1][1], boundingBox[0][0]:boundingBox[1][0]]
    cntBB = cnt2BoundingBox(cnt,boundingBox)
    #miniFrameBkg = miniFrame.copy()
    # bkgSample = sampleBkg(cntBB, miniFrameBkg)
    pixelsInBB = getPixelsList(cntBB, np.abs(boundingBox[0][0] - boundingBox[1][0]), np.abs(boundingBox[0][1] - boundingBox[1][1]))
    pixelsInFullF = pixelsInBB + np.asarray([boundingBox[0][1], boundingBox[0][0]])
    pixelsInFullFF = np.ravel_multi_index([pixelsInFullF[:,0], pixelsInFullF[:,1]],(height,width))
    return boundingBox, miniFrame, pixelsInFullFF, estimated_body_length

def getBlobsInfoPerFrame(frame, contours):
    boundingBoxes = []
    miniFrames = []
    centroids = []
    areas = []
    pixels = []
    estimated_body_lengths = []

    for i, cnt in enumerate(contours):
        boundingBox, miniFrame, pixelsInFullF, estimated_body_length = getMiniFrame(frame, cnt)
        #bounding boxes
        boundingBoxes.append(boundingBox)
        # miniframes
        miniFrames.append(miniFrame)
        # centroids
        centroids.append(getCentroid(cnt))
        # areas
        areas.append(cv2.contourArea(cnt))
        # pixels lists
        pixels.append(pixelsInFullF)
        # estimated body lengths list
        estimated_body_lengths.append(estimated_body_length)

    return boundingBoxes, miniFrames, centroids, areas, pixels, estimated_body_lengths

def blobExtractor(segmentedFrame, frame, minArea, maxArea):
    contours, hierarchy = cv2.findContours(segmentedFrame,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
    # Filter contours by size
    goodContoursFull = filterContoursBySize(contours,minArea, maxArea)
    # get contours properties
    boundingBoxes, miniFrames, centroids, areas, pixels, estimated_body_lengths = getBlobsInfoPerFrame(frame, goodContoursFull)

    return boundingBoxes, miniFrames, centroids, areas, pixels, goodContoursFull, estimated_body_lengths

def segmentVideo(frame, minThreshold, maxThreshold, bkg, ROI, useBkg):
    """Applies background substraction if requested and thresholds image
    :param frame: original frame normalised by the mean. Must be float32
    :param minThreshold: minimum intensity threshold (0-255)
    :param maxThreshold: maximum intensity threshold (0-255)
    :param bkg: background frame (normalised by mean???). Must be float32
    :param mask: boolean mask of region of interest where thresholding is performed. uint8, 255 valid, 0 invalid.
    :param useBkg: boolean determining if background subtraction is performed
    """
    if useBkg:
        frame = cv2.absdiff(bkg,frame) #only step where frame normalization is important, because the background is normalised

    frameSegmented = cv2.inRange(frame * (255.0/frame.max()), minThreshold, maxThreshold) #output: 255 in range, else 0
    # print("frame segmented frame ", frameSegmented.shape)
    # print("ROI shape: ", ROI.shape)
    frameSegmentedMasked = cv2.bitwise_and(frameSegmented,frameSegmented, mask=ROI) #Applying the mask
    return frameSegmentedMasked
