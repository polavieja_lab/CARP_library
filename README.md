# CARP library

This git repository includes different scripts used to build the CARP library,
to test different neural networks with images from CARP and to test the core
part of [idtracker.ai](http://idtracker.ai/) using images from CARP.

## Getting Started


### Prerequisites

Some part of the code requires having [idtracker.ai](http://idtracker.ai/) installed.
Use this scripts inside of the Conda environment created
while installing [idtracker.ai](http://idtracker.ai/).

### Installing

Check [the idtracker.ai repository](https://gitlab.com/polavieja_lab/idtrackerai)
for its installation.


## Authors

* **Francisco Romero-Ferrero**
* **Mattia G. Bergomi**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
