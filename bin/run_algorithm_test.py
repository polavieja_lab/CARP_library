from __future__ import absolute_import, division, print_function
# Import standard libraries
import os
import numpy as np
import time
import pandas as pd
from pprint import pprint
# Import third party libraries
# Import application/library specifics

from carp.experiments.tests_utils import TestConfig
from carp.experiments.track_synthetic_video import track_synthetic_video
from carp import Dataset

# seed numpy
np.random.seed(0)

# This script comes from the branch "algorithm_tests_with_library"
# in idtracker.ai repository


def check_if_repetition_has_been_computed(results_data_frame, job_config,
                                          group_size, frames_in_video,
                                          scale_parameter, shape_parameter,
                                          repetition):

    return len(results_data_frame.query('test_name == @job_config.test_name' +
                                        ' & CNN_model == @job_config.CNN_model' +
                                        ' & knowledge_transfer_flag == @job_config.knowledge_transfer_flag' +
                                        ' & knowledge_transfer_folder == @job_config.knowledge_transfer_folder' +
                                        ' & IMDB_codes == @job_config.IMDB_codes' +
                                        ' & ids_codes == @job_config.ids_codes' +
                                        ' & group_size == @group_size' +
                                        ' & frames_in_video == @frames_in_video' +
                                        ' & scale_parameter == @scale_parameter' +
                                        ' & shape_parameter == @shape_parameter'
                                        ' & repetition == @repetition')) != 0


def get_repetition_path(job_config, group_size, frames_in_video,
                        scale_parameter, shape_parameter, repetition):
    print("\n********** group size %i - frames_in_video %i - \
          scale_parameter %s -  shape_parameter %s - repetition %i ********"
          % (group_size, frames_in_video, str(scale_parameter),
             str(shape_parameter), repetition))
    return os.path.join(job_config.condition_path,
                        'group_size_' + str(group_size),
                        'num_frames_' + str(frames_in_video),
                        'scale_parameter_' + str(scale_parameter),
                        'shape_parameter_' + str(shape_parameter),
                        'repetition_' + str(repetition))


def compute_th_fragments_stats(scale_parameter, shape_parameter):
    mean_number_of_frames_per_fragment = shape_parameter * scale_parameter
    sigma_number_of_frames_per_fragment = \
        np.sqrt(shape_parameter * scale_parameter ** 2)
    print("mean_number_of_frames_per_fragment %.2f" %
          mean_number_of_frames_per_fragment)
    print("sigma_number_of_frames_per_fragment %.2f" %
          sigma_number_of_frames_per_fragment)
    return mean_number_of_frames_per_fragment,\
        sigma_number_of_frames_per_fragment


def save_results_data_frame(successful,
                            results_data_frame,
                            job_config,
                            video,
                            list_of_global_fragments,
                            group_size,
                            frames_in_video,
                            scale_parameter,
                            shape_parameter,
                            mean_number_of_frames_per_fragment,
                            sigma_number_of_frames_per_fragment,
                            repetition,
                            results_data_frame_path):
    results_data_frame = \
        results_data_frame.append({'date': time.strftime("%c"),
            'cluster': int(job_config.cluster),
            'test_name': job_config.test_name,
            'CNN_model': job_config.CNN_model,
            'knowledge_transfer_flag': job_config.knowledge_transfer_flag,
            'knowledge_transfer_folder': job_config.knowledge_transfer_folder,
            'IMDB_codes': job_config.IMDB_codes,
            'ids_codes': job_config.ids_codes,
            'group_size': int(group_size),
            'frames_in_video': int(frames_in_video),
            'scale_parameter': scale_parameter,
            'shape_parameter': shape_parameter,
            'mean_number_of_frames_per_fragment': mean_number_of_frames_per_fragment,
            'sigma_number_of_frames_per_fragment': sigma_number_of_frames_per_fragment,
            'repetition': int(repetition),
            'protocol': video.protocol if successful else None,
            'overall_P2': video.overall_P2 if successful else None,
            'individual_accuracy': video.gt_accuracy['individual_accuracy'] if successful else None,
            'accuracy': video.gt_accuracy['accuracy'] if successful else None,
            'individual_accuracy_assigned': video.gt_accuracy['individual_accuracy_assigned'] if successful else None,
            'accuracy_assigned': video.gt_accuracy['accuracy_assigned'] if successful else None,
            'individual_accuracy_in_accumulation': video.gt_accuracy['individual_accuracy_in_accumulation'] if successful else None,
            'accuracy_in_accumulation': video.gt_accuracy['accuracy_in_accumulation'] if successful else None,
            'individual_accuracy_after_accumulation': video.gt_accuracy['individual_accuracy_after_accumulation'] if successful else None,
            'accuracy_after_accumulation': video.gt_accuracy['accuracy_after_accumulation'] if successful else None,
            'crossing_detector_accuracy': video.gt_accuracy['crossing_detector_accuracy'] if successful else None,
            'individual_fragments_lengths': video.individual_fragments_lenghts,
            'number_of_global_fragments': list_of_global_fragments.number_of_global_fragments,
            'number_of_fragments': video.individual_fragments_stats['number_of_fragments'] if successful else None,
            'number_of_crossing_fragments': video.individual_fragments_stats['number_of_crossing_fragments'] if successful else None,
            'number_of_individual_fragments': video.individual_fragments_stats['number_of_individual_fragments'] if successful else None,
            'number_of_individual_fragments_not_in_a_global_fragment': video.individual_fragments_stats['number_of_individual_fragments_not_in_a_global_fragment'] if successful else None,
            'number_of_not_accumulable_individual_fragments': video.individual_fragments_stats['number_of_not_accumulable_individual_fragments'] if successful else None,
            'number_of_globally_accumulated_individual_blobs': video.individual_fragments_stats['number_of_globally_accumulated_individual_blobs'] if successful else None,
            'number_of_partially_accumulated_individual_fragments': video.individual_fragments_stats['number_of_partially_accumulated_individual_fragments'] if successful else None,
            'number_of_blobs': video.individual_fragments_stats['number_of_blobs'] if successful else None,
            'number_of_crossing_blobs': video.individual_fragments_stats['number_of_crossing_blobs'] if successful else None,
            'number_of_individual_blobs': video.individual_fragments_stats['number_of_individual_blobs'] if successful else None,
            'number_of_individual_blobs_not_in_a_global_fragment': video.individual_fragments_stats['number_of_individual_blobs_not_in_a_global_fragment'] if successful else None,
            'number_of_not_accumulable_individual_blobs': video.individual_fragments_stats['number_of_not_accumulable_individual_blobs'] if successful else None,
            'number_of_globally_accumulated_individual_fragments': video.individual_fragments_stats['number_of_globally_accumulated_individual_fragments'] if successful else None,
            'number_of_partially_accumulated_individual_blobs': video.individual_fragments_stats['number_of_partially_accumulated_individual_blobs'] if successful else None,
            'first_accumulation_time': video.first_accumulation_time if successful else None,
            'pretraining_time': video.pretraining_time if successful else None,
            'second_accumulation_time': video.second_accumulation_time if successful else None,
            'assignment_time': video.assignment_time if successful else None,
            'total_time': video.total_time if successful else None
             }, ignore_index=True)
    results_data_frame.to_pickle(results_data_frame_path)
    return results_data_frame


def main(args):
    cluster = args.cluster
    test_number = args.test_number
    carp_folder = args.carp_folder
    save_folder = args.save_folder
    tests_data_frame_path = os.path.join(save_folder,
                                         'tests_data_frame.pkl')
    results_data_frame_path = os.path.join(save_folder,
                                           'results_data_frame.pkl')

    tests_data_frame = pd.read_pickle(tests_data_frame_path)
    test_dictionary = tests_data_frame.loc[test_number].to_dict()
    pprint(test_dictionary)

    job_config = TestConfig(cluster=cluster,
                            test_dictionary=test_dictionary,
                            save_folder=save_folder)
    job_config.create_folders_structure()

    if os.path.isfile(results_data_frame_path):
        print("results_data_frame.pkl already exists \n")
        results_data_frame = pd.read_pickle(results_data_frame_path)
    else:
        print("results_data_frame.pkl does not exist \n")
        results_data_frame = pd.DataFrame()

    dataset = Dataset(IMDB_codes=job_config.IMDB_codes,
                      ids_codes=job_config.ids_codes,
                      data_folder=carp_folder)
    dataset.loadIMDBs()
    print("images shape, ", dataset.images.shape)

    for repetition in job_config.repetitions:
        for group_size in job_config.group_sizes:
            for frames_in_video in job_config.frames_in_video:
                for scale_parameter in job_config.scale_parameter:
                    for shape_parameter in job_config.shape_parameter:
                        repetition_path = get_repetition_path(job_config,
                                                              group_size,
                                                              frames_in_video,
                                                              scale_parameter,
                                                              shape_parameter,
                                                              repetition)

                        mean_number_of_frames_per_fragment,\
                            sigma_number_of_frames_per_fragment =\
                            compute_th_fragments_stats(scale_parameter,
                                                       shape_parameter)

                        already_computed = False
                        if os.path.isfile(results_data_frame_path):
                            already_computed = \
                                check_if_repetition_has_been_computed(
                                    results_data_frame,
                                    job_config, group_size,
                                    frames_in_video,
                                    scale_parameter,
                                    shape_parameter,
                                    repetition)

                        if already_computed:
                            print("The algorithm with this comditions has \
                                  been already tested")
                        else:
                            successful, \
                                video, \
                                list_of_blobs, \
                                list_of_fragments, \
                                list_of_global_fragments, _, \
                                list_of_blobs_config = \
                                track_synthetic_video(repetition_path,
                                                      group_size,
                                                      scale_parameter,
                                                      shape_parameter,
                                                      repetition,
                                                      dataset,
                                                      frames_in_video)

                            results_data_frame = save_results_data_frame(successful,
                                                    results_data_frame,
                                                    job_config,
                                                    video,
                                                    list_of_global_fragments,
                                                    group_size,
                                                    frames_in_video,
                                                    scale_parameter,
                                                    shape_parameter,
                                                    mean_number_of_frames_per_fragment,
                                                    sigma_number_of_frames_per_fragment,
                                                    repetition,
                                                    results_data_frame_path)
    return results_data_frame


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cluster", type=bool,
                        help="Set to 1 if the experiments are going to be \
                        executed in the cluster")
    parser.add_argument("-tn", "--test_number", type=int, default=None,
                        help="Test number from the tests_data_frame.pkl")
    parser.add_argument("-cf", "--carp_folder", type=str, default=False,
                        help="Folder where the CARP datasets are stored")
    parser.add_argument("-sf", "--save_folder", type=str, default='./',
                        help="Folder where to save the results")
    args = parser.parse_args()

    from tests_data_frame import tests_data_frame
    tests_data_frame(save_folder=args.save_folder)
    results_data_frame = main(args)
