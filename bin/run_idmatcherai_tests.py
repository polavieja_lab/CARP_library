from __future__ import absolute_import, division, print_function
import os
import numpy as np
from carp.experiments.track_synthetic_video import track_synthetic_video
from carp import Dataset
from idmatcherai.idmatcherai import match_identities
from idmatcherai.idmatcherai_plotter import plot_results
from idtrackerai.groundtruth_utils.compute_groundtruth_statistics import \
    get_permutation_of_identities


def load_tracking_session(video_path):
    session = os.path.join(video_path, 'session')
    video = np.load(os.path.join(session, 'video_object.npy')).item()
    list_of_blobs = np.load(os.path.join(session, 'preprocessing',
                                         'blobs_collection.npy')).item()
    groundtruth = np.load(os.path.join(video_path,
                                       '_groundtruth.npy')).item()
    return video, list_of_blobs, groundtruth


def get_identities_permutations(video, list_of_blobs, groundtruth):
    blobs_in_video_groundtruth = \
        groundtruth.blobs_in_video[groundtruth.start:groundtruth.end]
    blobs_in_video = \
        list_of_blobs.blobs_in_video[groundtruth.start:groundtruth.end]
    identities_dictionary_permutation = \
        get_permutation_of_identities(video.first_frame_first_global_fragment,
                                      blobs_in_video_groundtruth,
                                      blobs_in_video)
    print("identities in GT to identities in video: ",
          identities_dictionary_permutation)
    return identities_dictionary_permutation


def permute_results(results, id_perm_groundtruth_A, id_perm_groundtruth_B):
    def permute_matrix(matrix, perm_A, perm_B):
        matrix = matrix[perm_A, :]
        matrix = matrix[:, perm_B]
        return matrix

    def permute_vector(vector, perm_B):
        # in vector every entry is a fragment so it is B
        vector = vector[perm_B]
        return vector

    def permute_dictionary(dictionary, perm_A, perm_B):
        # in dictionary key = (fragment or B) and value = (network or A)
        permuted_dictionary = {}
        for key in dictionary.keys():
            permuted_dictionary[perm_B.index(key-1)+1] = \
                perm_A.index(dictionary[key]-1)+1
        return permuted_dictionary

    def permute_matching_results(matching_results, perm_A, perm_B, all=True):
        matching_results['confusion_matrix'] = \
            permute_matrix(matching_results['confusion_matrix'],
                           perm_A, perm_B)
        matching_results['frequencies_matrix'] = \
            permute_matrix(matching_results['frequencies_matrix'],
                           perm_A, perm_B)
        if all:
            matching_results['certainties'] = \
                permute_vector(matching_results['certainties'], perm_B)
            matching_results['matching_identities'] = \
                permute_dictionary(matching_results['matching_identities'],
                                   perm_A, perm_B)
        return matching_results

    perm_A = map(lambda x: x-1, id_perm_groundtruth_A.values())
    perm_B = map(lambda x: x-1, id_perm_groundtruth_B.values())

    results['matching_results_A_B'] = \
        permute_matching_results(results['matching_results_A_B'],
                                 perm_A, perm_B)
    results['matching_results_B_A'] = \
        permute_matching_results(results['matching_results_B_A'],
                                 perm_B, perm_A)
    results['matching_results'] = \
        permute_matching_results(results['matching_results'],
                                 perm_A, perm_B, all=False)
    return results


def main(args):
    track = TRACK
    carp_folder = args.carp_folder
    save_folder = args.save_folder
    group_size = GROUP_SIZE
    scale_parameter = SCALE_PARAMETER
    shape_parameter = SHAPE_PARAMETER
    repetition = REPETITION
    frames_in_video = FRAMES_IN_VIDEO

    dataset = Dataset(IMDB_codes=IMDB_CODES,
                      ids_codes=IDS_CODES,
                      data_folder=carp_folder)
    dataset.loadIMDBs()
    print("images shape, ", dataset.images.shape)
    # Track video A
    video_path_A = os.path.join(save_folder, 'videoA')
    if track:
        successful, video, list_of_blobs, list_of_fragments,\
            list_of_global_fragments, groundtruth, list_of_blobs_config = \
            track_synthetic_video(video_path_A, group_size, scale_parameter,
                                  shape_parameter, repetition, dataset,
                                  frames_in_video, starting_frame=0)
    else:
        video, list_of_blobs, groundtruth = load_tracking_session(video_path_A)
    id_perm_groundtruth_A = get_identities_permutations(video, list_of_blobs,
                                                        groundtruth)
    # Track video B
    video_path_B = os.path.join(save_folder, 'videoB')
    if track:
        successful, video, list_of_blobs, list_of_fragments,\
            list_of_global_fragments, groundtruth, list_of_blobs_config = \
            track_synthetic_video(video_path_B, group_size, scale_parameter,
                                  shape_parameter, repetition, dataset,
                                  frames_in_video, starting_frame=12500)
    else:
        video, list_of_blobs, groundtruth = load_tracking_session(video_path_B)
    id_perm_groundtruth_B = get_identities_permutations(video, list_of_blobs,
                                                        groundtruth)
    # Match identities
    session_A = os.path.join(video_path_A, 'session')
    session_B = os.path.join(video_path_B, 'session')
    results = match_identities(session_A, session_B, save_folder,
                               plot_results_flag=False)
    plot_results(results, save_folder)
    results_permuted = permute_results(results, id_perm_groundtruth_A,
                                       id_perm_groundtruth_B)
    plot_results(results_permuted, save_folder)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-cf", "--carp_folder", type=str, default=False,
                        help="Folder where the CARP datasets are stored")
    parser.add_argument("-sf", "--save_folder", type=str, default='./',
                        help="Folder where to save the results")
    args = parser.parse_args()

    TRACK = True
    GROUP_SIZE = 25
    SCALE_PARAMETER = 500
    SHAPE_PARAMETER = .3
    REPETITION = 1
    FRAMES_IN_VIDEO = 1000
    IMDB_CODES = 'J'
    IDS_CODES = 'a'
    main(args)
