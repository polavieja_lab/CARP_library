# Import standard libraries
import os
import glob
import numpy as np
# Import third party libraries
import cv2
from pprint import pprint
import h5py
from carp.generate.segmentation import segment
from carp.generate.get_portraits import portrait
from carp.generate.video_utils import getSegmPaths, checkBkg
from carp.generate.GUI_utils import selectDir, getInput, selectOptions,\
                                    ROISelectorPreview_library,\
                                    selectPreprocParams_library,\
                                    playFragmentation_library
from carp.generate.utils import portraitsToIMDB, retrieveInfoLib, assignCenters
from carp.generate.py_utils import scanFolder, getExistentFiles_library,\
                                  loadFile, natural_sort, createFolder

if __name__ == '__main__':
    cv2.namedWindow('Bars') #FIXME If we do not create the "Bars" window here we have the "Bad window error"...

    ''' ************************************************************************
    Selecting library directory
    ************************************************************************ '''
    libPath = selectDir('./')
    ageInDpf, subDirs, strain = retrieveInfoLib(libPath)
    # subDirs = subDirs[:4]
    print('ageInDpf, ', ageInDpf)
    print('strain, ', strain)
    group = 0
    labelsIMDB = []
    imagesIMDB = []
    centroidsIMDB = []
    areasIMDB = []
    numImagesList = []
    preprocessing_minAreas = []
    preprocessing_maxAreas = []
    preprocessing_minThreshold = []
    preprocessing_maxThreshold = []
    numIndivIMDB = 0
    totalNumImages = 0
    setParams = getInput('Set preprocessing parameters','Do you want to set the parameters for the preprocessing of each video? ([y]/n)')
    runPreproc = getInput('Run preprocessing','Do you want to run the preprocessing? ([y]/n)')
    buildLib = getInput('Build library','Do you want to build the library? ([y]/n)')
    if buildLib == 'y' or buildLib == '':
        library_part = getInput('Select library part','Which library part do you wanna build? ([a]/b)')
        if library_part == 'a':
            video_to_save = 1
        elif library_part == 'b':
            video_to_save = 2
        images_type = getInput('Select images type','Which kind of images do you wanna save? ([unprocessed]/preprocessed)')
    print(subDirs)
    for i, subDir in enumerate(subDirs):
        print('-----------------------')
        print('preprocessing ', subDir)

        ''' Path to video/s '''
        path = libPath + '/' + subDir
        extensions = ['.avi', '.mp4']
        videoPath = natural_sort([v for v in os.listdir(path) if os.path.isfile(path +'/'+ v) if any( ext in v for ext in extensions)])[0]
        videoPath = path + '/' + videoPath
        videoPaths = scanFolder(videoPath)
        createFolder(videoPath)
        frameIndices, segmPaths = getSegmPaths(videoPaths)

        ''' ************************************************************************
        Set preprocessing parameters
        ************************************************************************ '''
        if setParams == 'y' or setParams == '':
            setThisPrepParams = getInput('Set preprocessing parameters','Do you want to set parameters for this subDir ('+ subDir +')? ([y]/n)')
            print('setThisPrepParams', setThisPrepParams)
            if setThisPrepParams == 'n':
                continue
            elif setThisPrepParams == 'y' or setThisPrepParams == '':
                ''' ************************************************************************
                GUI to select the preprocessing parameters
                *************************************************************************'''
                prepOpts = selectOptions(['bkg', 'ROI'], None, text = 'Do you want to do BKG or select a ROI?  ')
                useBkg = prepOpts['bkg']
                useROI =  prepOpts['ROI']

                #Check for preexistent files generated during a previous session. If they
                #exist and one wants to keep them they will be loaded
                processesList = ['ROI', 'bkg', 'preprocparams', 'segmentation','fragments','identities_images_areas_centroids']
                existentFiles, srcSubFolder = getExistentFiles_library(videoPath, processesList, segmPaths)
                print('List of processes finished, ', existentFiles)
                print('\nSelecting files to load from previous session...')
                loadPreviousDict = selectOptions(processesList, existentFiles, text='Steps already processed in this video \n (check to load from ' + srcSubFolder + ')')

                usePreviousROI = loadPreviousDict['ROI']
                usePreviousBkg = loadPreviousDict['bkg']
                usePreviousPrecParams = loadPreviousDict['preprocparams']
                print('useROI', useROI)
                print('usePreviousROI set to ', usePreviousROI)
                print('usePreviousBkg set to ', usePreviousBkg)
                print('usePreviousPrecParams set to ', usePreviousPrecParams)

                ''' ROI selection/loading '''
                width, height, mask, centers = ROISelectorPreview_library(videoPaths, useROI, usePreviousROI, numSegment=0)
                ''' BKG computation/loading '''
                bkg = checkBkg(videoPaths, useBkg, usePreviousBkg, 0, width, height)

                ''' Selection/loading preprocessing parameters '''
                preprocParams = selectPreprocParams_library(videoPaths, usePreviousPrecParams, width, height, bkg, mask, useBkg, frameIndices)
                print('The video will be preprocessed according to the following parameters: ', preprocParams)

                cv2.namedWindow('Bars')

        ''' ************************************************************************
        Preprocessing
        ************************************************************************ '''
        if runPreproc == 'y' or runPreproc == '':
            processesList = ['ROI', 'bkg', 'preprocparams', 'segmentation','fragmentation','portraits']
            loadPreviousDict, srcSubFolder = getExistentFiles_library(videoPath, processesList, segmPaths)
            useBkg = int(loadPreviousDict['bkg'])
            useROI = loadPreviousDict['ROI']
            preprocParams= loadFile(videoPaths[0], 'preprocparams',hdfpkl='pkl')
            numAnimalsInGroup = preprocParams['numAnimals']

            ''' ************************************************************************
            Segmentation
            ************************************************************************ '''
            cv2.waitKey(1)
            cv2.destroyAllWindows()
            cv2.waitKey(1)
            centers = loadFile(videoPaths[0], 'centers')
            mask = np.asarray(loadFile(videoPaths[0], 'ROI'))
            if useBkg:
                bkg = loadFile(videoPaths[0], 'bkg', hdfpkl='pkl')
            else:
                bkg = None
            preprocParams= loadFile(videoPaths[0], 'preprocparams',hdfpkl = 'pkl')
            if not int(loadPreviousDict['segmentation']):
                EQ = 0
                print('The preprocessing parameters dictionary loaded is ', preprocParams)
                segment(videoPaths, preprocParams, mask, centers, useBkg, bkg, EQ)

            ''' ************************************************************************
            Fragmentation
            *************************************************************************'''
            ''' Group and number of individuals '''
            # We assign the group number in a iterative way so that if one group is missing the labels of the IMDB are still iterative
            nameElements = subDir.split('_')
            if 'camera' in nameElements: ### old libraries ()
                transform = 'rotation'
                video = int(nameElements[3])
                if video == 1 and i != 0:
                    group += 1
                if video == 2:
                    numIndivIMDB += numAnimalsInGroup
            elif len(nameElements) == 3: ### new libraries (CARP)
                transform = 'translation'
                video = int(nameElements[2])
                if video == 1 and i != 0:
                    group += 1
                if video == 2:
                    numIndivIMDB += numAnimalsInGroup
            elif len(nameElements) == 2:
                transform = 'none'
                video = 1
                group += 1
                numIndivIMDB += numAnimalsInGroup

            print('************************************************************')
            print('Video ', video)
            print('Transform, ', transform)
            print('Group, ', group)
            print('numAnimalsInGroup, ', numAnimalsInGroup)
            print('numIndivIMDB, ', numIndivIMDB)
            print('************************************************************')

            if not int(loadPreviousDict['fragmentation']):
                print('centers, ', centers)
                centers = centers.drop_duplicates()
                centers = centers.reset_index(drop=True)
                print('centers, ', centers)
                if len(centers) != numAnimalsInGroup:
                    print('centers, ', centers)
                    print('numAnimalsInGroup, ', numAnimalsInGroup)
                    raise ValueError('The number of centers to assign identities is different to the number of animals in the group')
                dfGlobal = assignCenters(segmPaths,centers,video,transform = transform)
                print(dfGlobal.columns)
                playFragmentation_library(videoPaths,segmPaths,dfGlobal,visualize=False)
                cv2.waitKey(1)
                cv2.destroyAllWindows()
                cv2.waitKey(1)

            ''' ************************************************************************
            Portraying
            ************************************************************************ '''
            if not int(loadPreviousDict['portraits']):
                videoInfo = loadFile(videoPaths[0], 'videoInfo')
                height = videoInfo['height']
                width = videoInfo['width']
                portrait(segmPaths, dfGlobal, height, width)

        if buildLib == 'y' or buildLib == '':
            ''' ************************************************************************
            Build images and labels array
            ************************************************************************ '''
            preprocParams= loadFile(videoPaths[0], 'preprocparams')
            # preprocParams = preprocParams.to_dict()[0]
            numAnimalsInGroup = preprocParams['numAnimals']
            print('numAnimalsInGroup, ', numAnimalsInGroup)
            ''' Group and number of individuals '''
            # We assign the group number in a iterative way so that if one group is missing the labels of the IMDB are still iterative
            nameElements = subDir.split('_')
            if 'camera' in nameElements: ###NOTE this only works if the subDirs list alternates between camera 1 and 2 every time
                transform = 'rotation'
                video = int(nameElements[3])
                if video == 1 and i != 0:
                    group += 1
                if video == 2:
                    numIndivIMDB += numAnimalsInGroup
            elif len(nameElements) == 3:
                transform = 'translation'
                video = int(nameElements[2])
                if video == 1 and i != 0:
                    group += 1
                if video == 2:
                    numIndivIMDB += numAnimalsInGroup
            elif len(nameElements) == 2:
                transform = 'none'
                video = 1
                if i != 0:
                    group += 1
                numIndivIMDB += numAnimalsInGroup

            if video_to_save == video:
                print('Video, ', video)
                print('transform, ', transform)
                print('numIndivIMDB, ', numIndivIMDB)
                print('group, ', group)
                print('Loading identities')
                identities = loadFile(videoPaths[0], 'identities')
                print('Loading images')
                if images_type == 'preprocessed':
                    images = loadFile(videoPaths[0], 'preprocessed_images')
                elif images_type == 'unprocessed' or images_type == '':
                    images = loadFile(videoPaths[0], 'unprocessed_images')
                print('Loading centroids')
                centroids = loadFile(videoPaths[0], 'centroids')
                print('Loading areas')
                areas = loadFile(videoPaths[0], 'areas')

                print('Extracting information from video')
                labels, images, centroids, areas = portraitsToIMDB(identities, images, centroids, areas, numAnimalsInGroup, group)
                print('images shape, ', images.shape)
                print('labels shape, ', labels.shape)
                print('labels, ', np.unique(labels))
                print('images per indiv, ', [np.sum(labels==i) for i in np.unique(labels)])


                labelsIMDB.append(labels)
                imagesIMDB.append(images)
                centroidsIMDB.append(centroids)
                areasIMDB.append(areas)
                preprocessing_minAreas.extend([preprocParams['minArea']] * numAnimalsInGroup)
                preprocessing_maxAreas.extend([preprocParams['maxArea']] * numAnimalsInGroup)
                preprocessing_minThreshold.extend([preprocParams['minThreshold']] * numAnimalsInGroup)
                preprocessing_maxThreshold.extend([preprocParams['maxThreshold']] * numAnimalsInGroup)

                totalNumImages += labels.shape[0]

                numImagesList.append(labels.shape[0])

                images = None
                labels = None
                centroids = None
                areas = None

                print 'total number of images, ', numImagesList

    ''' ************************************************************************
    Save IMDB to hdf5 or jpg
    ************************************************************************ '''
    if buildLib == 'y' or buildLib == '':

        preprocParams= loadFile(videoPaths[0], 'preprocparams')
        # preprocParams = preprocParams.to_dict()[0]
        labelsIMDB = np.concatenate(labelsIMDB, axis = 0)
        imagesIMDB = np.concatenate(imagesIMDB, axis = 0)
        centroidsIMDB = np.concatenate(centroidsIMDB, axis = 0)
        areasIMDB = np.concatenate(areasIMDB, axis = 0)
        minimalNumImagesPerIndiv = int(np.min([np.sum(labelsIMDB == i) for i in np.unique(labelsIMDB)]))
        print '\n****************************************************************'
        print 'numIndivIMDB, ', numIndivIMDB
        print 'totalNumImages, ', totalNumImages
        print 'minNumImages, ', minimalNumImagesPerIndiv
        print 'labelsIMDB, ', np.unique(labelsIMDB)
        print 'num images per indiv, ', [np.sum(labelsIMDB == i) for i in np.unique(labelsIMDB)]

        nameDatabase =  strain + '_' + \
                        str(numIndivIMDB) + 'individuals_' + \
                        images_type + '_' + \
                        library_part

        if not os.path.exists(os.path.join(libPath, 'IMDBs', images_type)):
            os.makedirs(os.path.join(libPath, 'IMDBs', images_type))
            if images_type == 'unprocessed':
                os.makedirs(os.path.join(libPath, 'IMDBs', images_type, nameDatabase))
        if os.path.isfile(os.path.join(libPath, 'IMDBs', images_type, nameDatabase + '_0.hdf5')) and images_type == 'preprocessed':
            text = 'A IMDB already exist with this name (' + nameDatabase + '). Do you want to create a new one with a different name?'
            newName = getInput('Confirm selection','The IMDB already exist. Do you want to create a new one with a different name [y/n]?')
            if newName == 'y':
                nameDatabase = getInput('Insert new name: ','The current name is "' + nameDatabase + '"')
            elif newName == 'n':
                displayMessage('Overwriting IMDB','You are going to overwrite the current IMDB (' + nameDatabase + ').')
                for filename in glob.glob(libPath + '/IMDBs/' + nameDatabase + '_*.hdf5'):
                    os.remove(filename)
                if images_type == 'unprocessed':
                    folder_name = os.path.join(libPath, 'IMDBs', images_type, 'images')
                    os.remove(foldername)
            else:
                raise ValueError('Invalid string, it must be "y" or "n"')

        if images_type == 'preprocessed':
            f = h5py.File(os.path.join(libPath, 'IMDBs', images_type, nameDatabase + '_%i.hdf5'), driver='family')
            grp = f.create_group("database")

            dset1 = grp.create_dataset("labels", labelsIMDB.shape, dtype='i')
            if images_type == 'preprocessed':
                dset2 = grp.create_dataset("images", imagesIMDB.shape, dtype='f')
            dset3 = grp.create_dataset("centroids", centroidsIMDB.shape, dtype='f')
            dset4 = grp.create_dataset("areas", areasIMDB.shape, dtype='f')

            dset1[...] = labelsIMDB
            if images_type == 'preprocessed':
                dset2[...] = imagesIMDB
            dset3[...] = centroidsIMDB
            dset4[...] = areasIMDB

            grp.attrs['originalMatPath'] = libPath
            grp.attrs['numIndiv'] = numIndivIMDB
            grp.attrs['strain'] = strain
            grp.attrs['numImagesPerIndiv'] = minimalNumImagesPerIndiv
            grp.attrs['ageInDpf'] = ageInDpf
            grp.attrs['minArea'] = preprocessing_minAreas
            grp.attrs['maxArea'] = preprocessing_maxAreas
            grp.attrs['minThreshold'] = preprocessing_minThreshold
            grp.attrs['maxThreshold'] = preprocessing_maxThreshold

            pprint([item for item in grp.attrs.iteritems()])

            f.close()

        print 'Database saved as %s ' % nameDatabase

        if images_type == 'unprocessed':
            folder_name = os.path.join(libPath, 'IMDBs', images_type, nameDatabase)

            for label in np.unique(labelsIMDB):
                print('saving label %i' % label)
                label_folder_name = os.path.join(folder_name, '%i' %label)
                if not os.path.exists(label_folder_name):
                    os.makedirs(label_folder_name)
                for image_number, image in enumerate(imagesIMDB[labelsIMDB == label]):
                    image_path = os.path.join(label_folder_name, '%i_%i' %(label, image_number) + '.png')
                    cv2.imwrite(image_path,image)
