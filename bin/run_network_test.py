from __future__ import absolute_import, division, print_function
import os
import numpy as np
import pandas as pd
import time
from pprint import pprint
import logging.config
import yaml
from idtrackerai.network.identification_model.network_params import NetworkParams
from idtrackerai.network.identification_model.id_CNN import ConvNetwork
from idtrackerai.trainer import train
from idtrackerai.assigner import assign
from carp.experiments.tests_utils import TestConfig
from carp.experiments.network_tests_utils import check_if_repetition_has_been_computed_network,\
                                                sample_individuals, slice_dataset,\
                                                get_training_and_test_images,\
                                                standarize_images,\
                                                correct_loss_nan,\
                                                compute_test_accuracies
from carp import Dataset

# This script comes from the branch "network_tests_gt"
# in idtracker.ai repository


def setup_logging(default_path='logging.yaml', default_level=logging.INFO,
                  env_key='LOG_CFG', path_to_save_logs='./'):
    """Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        if os.path.exists(path_to_save_logs):
            logs_folder = os.path.join(path_to_save_logs, 'log_files')
            if not os.path.isdir(logs_folder):
                os.makedirs(logs_folder)
            config['handlers']['info_file_handler']['filename'] = \
                os.path.join(logs_folder, 'info.log')
            config['handlers']['error_file_handler']['filename'] = \
                os.path.join(logs_folder, 'error.log')
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

    logger = logging.getLogger(__name__)
    logger.propagate = True
    logger.setLevel("DEBUG")
    return logger


def update_results_data_frame(results_data_frame_path,
                              results_data_frame, job_config, group_size,
                              frames_in_video, frames_in_fragment, repetition,
                              store_validation_accuracy_and_loss_data,
                              accuracy, individual_accuracy,
                              training_time, testing_time):
    results_data_frame = \
        results_data_frame.append({
            'date': time.strftime("%c"),
            'cluster': int(job_config.cluster),
            'test_name': job_config.test_name,
            'CNN_model': job_config.CNN_model,
            'knowledge_transfer_flag': job_config.knowledge_transfer_flag,
            'knowledge_transfer_folder': job_config.knowledge_transfer_folder,
            'pretraining_flag': job_config.pretraining_flag,
            'percentage_of_frames_in_pretaining': job_config.percentage_of_frames_in_pretaining,
            'only_accumulate_one_fragment': job_config.only_accumulate_one_fragment,
            'train_filters_in_accumulation': bool(job_config.train_filters_in_accumulation),
            'accumulation_certainty': job_config.accumulation_certainty,
            'IMDB_codes': job_config.IMDB_codes,
            'ids_codes': job_config.ids_codes,
            'group_size': int(group_size),
            'frames_in_video': int(frames_in_video),
            'frames_per_fragment': int(frames_in_fragment),
            'repetition': int(repetition),
            'number_of_fragments': None,
            'proportion_of_accumulated_fragments': None,
            'number_of_not_assigned_blobs': None,
            'test_accuracy': accuracy,
            'training_individual_accuracy': store_validation_accuracy_and_loss_data.individual_accuracy[-1],
            'validation_individual_accuracy': store_validation_accuracy_and_loss_data.individual_accuracy[-1],
            'test_individual_accuracy': individual_accuracy,
            'training_time': training_time,
            'test_time': testing_time,
            'proportion_of_identity_repetitions': None,
            'proportion_of_identity_shifts_in_accumulated_frames': None,
            'pretraining_time': None,
            'accumulation_time': None,
            'assignation_time': None,
            'total_time': training_time + testing_time,
             }, ignore_index=True)
    results_data_frame.to_pickle(results_data_frame_path)
    return results_data_frame


def main(args):
    cluster = args.cluster
    test_number = args.test_number
    carp_folder = args.carp_folder
    save_folder = args.save_folder
    tests_data_frame_path = os.path.join(save_folder,
                                         'tests_data_frame.pkl')
    results_data_frame_path = os.path.join(save_folder,
                                           'results_data_frame.pkl')

    # Read tests dataframe
    tests_data_frame = pd.read_pickle(tests_data_frame_path)
    test_dictionary = tests_data_frame.loc[test_number].to_dict()
    pprint(test_dictionary)
    # Get job_configuration and create folders structure
    job_config = TestConfig(cluster=cluster,
                                  test_dictionary=test_dictionary,
                                  save_folder=save_folder)
    job_config.create_folders_structure()
    # set log config
    logger = setup_logging(path_to_save_logs=job_config.condition_path)
    logger.info("Log files saved in %s" % job_config.condition_path)
    # Open or create results data_frame
    if os.path.isfile(results_data_frame_path):
        print("results_data_frame.pkl already exists \n")
        results_data_frame = pd.read_pickle(results_data_frame_path)
    else:
        print("results_data_frame.pkl does not exist \n")
        results_data_frame = pd.DataFrame()
    # Create test data frame
    dataset = Dataset(IMDB_codes=job_config.IMDB_codes,
                      ids_codes=job_config.ids_codes,
                      data_folder=carp_folder)
    dataset.loadIMDBs()
    print("images shape, ", dataset.images.shape)
    for group_size in job_config.group_sizes:
        for frames_in_video in job_config.frames_in_video:
            for frames_in_fragment in job_config.frames_per_individual_fragment:
                for repetition in job_config.repetitions:
                    save_folder = os.path.join(job_config.condition_path,
                                               'group_size_' + str(group_size),
                                               'num_frames_' + str(frames_in_video),
                                               'frames_in_fragment_' + str(frames_in_fragment),
                                               'repetition_' + str(repetition))

                    print("\n********** group size %i - frames_in_video %i - \
                          frames_in_fragment %i - repetition %i ********"
                          % (group_size, frames_in_video,frames_in_fragment,
                             repetition))
                    already_computed = False
                    if os.path.isfile(results_data_frame_path):
                        already_computed, index = \
                            check_if_repetition_has_been_computed_network(results_data_frame,
                                                                          job_config, group_size,
                                                                          frames_in_video, frames_in_fragment,
                                                                          repetition)
                        print("*** Already computed ", already_computed)
                    if not already_computed:
                        # Init random generation
                        np.random.seed(repetition)
                        # Select subset of individuals
                        individual_indices = sample_individuals(dataset,
                                                                group_size)
                        # Get images of the selected individuals
                        images, labels = slice_dataset(dataset.images,
                                                       dataset.labels,
                                                       individual_indices)
                        # Get images for training, validation and testing
                        images, labels, test_images, test_labels = \
                            get_training_and_test_images(
                                images, labels, frames_in_video,
                                dataset.minimum_number_of_images_per_animal)
                        images = standarize_images(images)
                        test_images = standarize_images(test_images)
                        # Init network
                        train_network_params = \
                            NetworkParams(group_size,
                                          cnn_model=job_config.CNN_model,
                                          learning_rate=0.005,
                                          keep_prob=1.0,
                                          use_adam_optimiser=False,
                                          scopes_layers_to_optimize=None,
                                          restore_folder=None,
                                          save_folder=save_folder,
                                          image_size=images.shape[1:] + (1,))
                        net = ConvNetwork(train_network_params)
                        net.restore()
                        # Training
                        print("\n***training")
                        start_time = time.time()
                        _, net, \
                            store_validation_accuracy_and_loss_data, \
                            store_training_accuracy_and_loss_data = \
                            train(None, None, net, images, labels,
                                  store_accuracy_and_error=True,
                                  check_for_loss_plateau=True,
                                  save_summaries=False,
                                  print_flag=True,
                                  plot_flag=False,
                                  global_step=0)
                        # Correct loss NaN if necessary
                        store_validation_accuracy_and_loss_data = \
                            correct_loss_nan(store_validation_accuracy_and_loss_data)
                        training_time = time.time() - start_time
                        # Testing
                        print("\n***testing")
                        start_time = time.time()
                        # Assign test images
                        assigner = assign(net, test_images, True)
                        testing_time = time.time() - start_time
                        # Compute test accuracies
                        individual_accuracy, accuracy = \
                            compute_test_accuracies(test_labels,
                                                    assigner._predictions)
                        # update results_data_frame
                        results_data_frame = update_results_data_frame(
                            results_data_frame_path,
                            results_data_frame,
                            job_config,
                            group_size,
                            frames_in_video,
                            frames_in_fragment,
                            repetition,
                            store_validation_accuracy_and_loss_data,
                            accuracy,
                            individual_accuracy,
                            training_time,
                            testing_time)
                        images, labels, test_images, test_labels = \
                            None, None, None, None


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cluster", type=bool,
                        help="Set to 1 if the experiments are going to be \
                        executed in the cluster")
    parser.add_argument("-tn", "--test_number", type=int, default=None,
                        help="Test number from the tests_data_frame.pkl")
    parser.add_argument("-cf", "--carp_folder", type=str, default=False,
                        help="Folder where the CARP datasets are stored")
    parser.add_argument("-sf", "--save_folder", type=str, default='./',
                        help="Folder where to save the results")
    args = parser.parse_args()

    from tests_data_frame import tests_data_frame
    tests_data_frame(save_folder=args.save_folder)
    main(args)
