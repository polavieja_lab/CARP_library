from __future__ import print_function, division
import numpy as np
from matplotlib import pyplot as plt
import cv2
import keras
from keras.applications.vgg16 import VGG16
from keras.applications.inception_v3 import InceptionV3
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers import Dropout
from keras.layers import Dense, GlobalAveragePooling2D, GlobalMaxPooling2D
from keras import backend as K
from carp.experiments.tests_utils import sample_individuals, slice_dataset,\
                                get_training_and_test_images
from idtrackerai.network.identification_model.get_data import \
    split_data_train_and_validation

from carp import Dataset


def resize_images(images, target_shape):
    print("resizing images...")
    images_resized = np.zeros((images.shape[0],) +
                              target_shape + (3,)).astype('float32')
    for i, x in enumerate(images):
        x = cv2.resize(x, target_shape, interpolation=cv2.INTER_LINEAR)
        x = np.tile(np.expand_dims(x, axis=2), (1, 1, 3))
        images_resized[i, ...] = x
    return images_resized


def get_model():
    input_tensor = Input(shape=(IMAGE_SIZE, IMAGE_SIZE, 3))
    # create the base pre-trained model
    base_model = VGG16(input_tensor=input_tensor,
                       weights='imagenet',
                       include_top=False)
    # base_model = InceptionV3(input_tensor=input_tensor,
    #                          weights='imagenet',
    #                          include_top=False)
    # add a global spatial average pooling layer
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    # x = Flatten(x)
    # let's add a fully-connected layer
    x = Dense(100, activation='relu')(x)
    # x = Dropout(.2)(x)
    # and a logistic layer -- let's say we have 200 classes
    predictions = Dense(NUM_CLASSES, activation='softmax')(x)
    # this is the model we will train
    model = Model(inputs=base_model.input, outputs=predictions)
    return base_model, model


def set_conv_layers_to_not_trainable(base_model):
    # first: train only the top layers (which were randomly initialized)
    # i.e. freeze all convolutional layers
    for layer in base_model.layers:
        layer.trainable = False


def compile_model(model):
    # compile the model (should be done *after* setting layers to
    # non-trainable)
    # optimizer = keras.optimizers.SGD(lr=0.0001,
    #                                  decay=1e-6,
    #                                  momentum=0.9,
    #                                  nesterov=True)
    optimizer = keras.optimizers.Adam(lr=0.0001,
                                      beta_1=0.9,
                                      beta_2=0.999,
                                      epsilon=None,
                                      decay=0.0)
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=optimizer,
                  metrics=['accuracy'])


def fit_model(model, x_train, y_train, x_test, y_test):
    # train the model on the new data for a few epochs
    callbacks = [keras.callbacks.EarlyStopping(monitor='val_loss',
                                               min_delta=0,
                                               patience=5,
                                               verbose=0,
                                               mode='auto')]
    #callbacks = None
    model.fit(x_train, y_train,
              batch_size=BATCH_SIZE,
              epochs=NUM_EPOCHS,
              verbose=1,
              validation_data=(x_test, y_test),
              callbacks=callbacks)
    return model


def test_model(model, x_test, y_test):
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])


def plot_dataset(x_train, y_train, rows):
    fig, ax_arr = plt.subplots(rows, rows)
    for i, ax in enumerate(ax_arr.flatten()):
        ax.set_title(str(np.argmax(y_train[i])))
        ax.imshow(x_train[i, ..., 0], interpolation=None)
    plt.show()


def standarize_images(images):
    # this standarization should change depending on the model loaded
    images -= np.mean(images)
    return images


def remove_extra_dimensions(training_dataset, validation_dataset):
    training_dataset.images = np.squeeze(training_dataset.images)
    validation_dataset.images = np.squeeze(validation_dataset.images)
    return training_dataset, validation_dataset


def to_on_hot(training_datset, validation_dataset, y_test):
    training_dataset.convert_labels_to_one_hot()
    validation_dataset.convert_labels_to_one_hot()
    y_test = keras.utils.to_categorical(y_test, NUM_CLASSES)
    return training_dataset, validation_dataset, y_test


if __name__ == '__main__':
    import argparse
    # constants
    BATCH_SIZE = 50
    NUM_CLASSES = 10
    NUM_EPOCHS = 1000
    IMAGE_SIZE = 56
    IMAGES_PER_ANIMAL = 3000

    parser = argparse.ArgumentParser()
    parser.add_argument("-cf", "--carp_folder", type=str, default=False,
                        help="Folder where the CARP datasets are stored")
    args = parser.parse_args()

    dataset = Dataset(IMDB_codes='J',
                      ids_codes='a',
                      data_folder=args.carp_folder)
    dataset.loadIMDBs()
    np.random.seed(1)  # repetition
    # Select subset of individuals
    individual_indices = sample_individuals(dataset,
                                            NUM_CLASSES)
    # Get images of the selected individuals
    images, labels = slice_dataset(dataset.images,
                                   dataset.labels,
                                   individual_indices)
    # standarize images
    images = standarize_images(images)
    # resize images to have a particular image size and 3 channels
    images = resize_images(images, target_shape=(IMAGE_SIZE, ) * 2)
    # split in train_validation and test sets
    x_train, y_train, x_test, y_test = \
        get_training_and_test_images(
            images, labels, IMAGES_PER_ANIMAL,
            dataset.minimum_number_of_images_per_animal)
    # split train_validations using idtrackerai helper as it does the 180
    # degree flip
    training_dataset, validation_dataset = \
        split_data_train_and_validation(NUM_CLASSES,
                                        x_train, y_train)
    # Remove one channel as the idtracker.ai helper adds one dimensions
    training_dataset, validation_dataset =\
        remove_extra_dimensions(training_dataset, validation_dataset)
    # Convert labels to one hot vectors
    training_dataset, validation_dataset, y_test = \
        to_on_hot(training_dataset, validation_dataset, y_test)
    # print summary of the different sets of images
    print("training:", training_dataset.images.shape,
          training_dataset.labels.shape,
          np.min(training_dataset.images), np.max(training_dataset.images))
    print("validation: ", validation_dataset.images.shape,
          validation_dataset.labels.shape,
          np.min(training_dataset.images), np.max(training_dataset.images))
    print("test: ", x_test.shape, y_test.shape,
          np.min(x_test), np.max(x_test))
    # get model
    base_model, model = get_model()
    # set conv layers to not trainable
    set_conv_layers_to_not_trainable(base_model)
    # compile model
    compile_model(model)
    # fit model
    model = fit_model(model, training_dataset.images, training_dataset.labels,
                      validation_dataset.images, validation_dataset.labels)
    # Store the model on disk
    model.save('tmp.h5')
    # In every test we will clear the session and reload the model to force
    # Learning_Phase values to change.
    print('DYNAMIC LEARNING_PHASE')
    K.clear_session()
    model = load_model('tmp.h5')
    test_model(model, x_test, y_test)
    print('STATIC LEARNING_PHASE = 0')
    K.clear_session()
    K.set_learning_phase(0)
    model = load_model('tmp.h5')
    test_model(model, x_test, y_test)
    print('STATIC LEARNING_PHASE = 1')
    K.clear_session()
    K.set_learning_phase(1)
    model = load_model('tmp.h5')
    test_model(model, x_test, y_test)
